package ru.pcconfig.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import ru.pcconfig.controller.utils.EmptyFieldException;
import ru.pcconfig.controller.utils.FilteredComponentList;
import ru.pcconfig.controller.utils.Util;
import ru.pcconfig.model.dao.ProcessorDAO;
import ru.pcconfig.model.dto.Processor;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Ruslan on 01.04.2017.
 */
public class ProcessorFiltersController extends ComponentFiltersController<Processor, ProcessorDAO>  {

    protected static final String CORE_AMOUNT_ERROR = "������ ��� ������ ���� ��������� ����";
    protected static final String MIN_FREQ_ERROR = "������ ��� ������ ���� ������� ����������";
    protected static final String MAX_TDP_ERROR = "������ ��� ������ ���� �������������� ����������";

//    @Autowired
//    ProcessorDAO processorDAO;

    @FXML
    private MenuButton menuButtonSocket;

    @FXML
    private TextField textFieldCoreAmount;

    @FXML
    private MenuButton menuButtonMemoryType;

    @FXML
    private CheckBox checkBoxGraphicCore;

    @FXML
    private TextField textFieldMinFreq;

    @FXML
    private TextField textFieldMaxTdp;

    @FXML
    private MenuButton menuButtonProducer;

    private FilteredComponentList<Processor> processorsFilteredBySocket = new FilteredComponentList();
    private FilteredComponentList<Processor> processorsFilteredByProducer = new FilteredComponentList();
    private FilteredComponentList<Processor> processorsFilteredByCoreAmount = new FilteredComponentList();
    private FilteredComponentList<Processor> processorsFilteredByMemoryType = new FilteredComponentList();
    private FilteredComponentList<Processor> processorsFilteredByGraphicCore = new FilteredComponentList();
    private FilteredComponentList<Processor> processorsFilteredByMinFreq = new FilteredComponentList();
    private FilteredComponentList<Processor> processorsFilteredByMaxTdp = new FilteredComponentList();


    @PostConstruct
    private void postConstruct() {
        initMenuButtonSocket();
        initMenuButtonProducer();
        initMenuButtonMemoryType();

        currentComponentList = dao.getAll();
        updateViewList();

    }

    protected void readCheckBoxGraphicCore() {
        if (checkBoxGraphicCore.isSelected()) {
            processorsFilteredByGraphicCore.setComponents(dao.getProcessorsByGraphicCore(true));
        } else
            processorsFilteredByGraphicCore.resetComponents();
    }

    protected void readControls() {
        readTextFieldMinFreq();
        readTextFieldCoreAmount();
        readTextFieldMaxTdp();
        readCheckBoxGraphicCore();
    }

    protected void readTextFieldMinFreq() {
        try {
            double minFreq = Util.readDoubleFormTextField(textFieldMinFreq);
            Util.throwExIfLessOrEq(minFreq, 0);
            processorsFilteredByMinFreq.setComponents(dao.getProcessorsByFrequency(minFreq));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_FREQ_ERROR);
        } catch (EmptyFieldException e) {
            processorsFilteredByMinFreq.resetComponents();
        }
    }


    protected void readTextFieldMaxTdp() {
        try {
            int maxTdp = Util.readIntFormTextField(textFieldMaxTdp);
            Util.throwExIfLessOrEq(maxTdp, 0);
            processorsFilteredByMaxTdp.setComponents(dao.getProcessorsByTdp(maxTdp));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MAX_TDP_ERROR);
        } catch (EmptyFieldException e) {
            processorsFilteredByMaxTdp.resetComponents();
        }
    }


    protected void readTextFieldCoreAmount() {
        try {
            int coreAmount = Util.readIntFormTextField(textFieldCoreAmount);
            Util.throwExIfLessOrEq(coreAmount, 0);
            processorsFilteredByCoreAmount.setComponents(dao.getProcessorsByCoreAmount(coreAmount));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, CORE_AMOUNT_ERROR);
        } catch (EmptyFieldException e) {
            processorsFilteredByCoreAmount.resetComponents();
        }
    }


    protected void initMenuButtonSocket() {
        initMenuButton(menuButtonSocket, processorsFilteredBySocket, new InitMenuButtonHelper() {
            @Override
            List<Processor> getFilteredComponents(String name) {
                return dao.getProcessorsBySocket(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableSockets();
            }
        });
    }



    protected void initMenuButtonMemoryType() {
        initMenuButton(menuButtonMemoryType, processorsFilteredByMemoryType, new InitMenuButtonHelper() {
            @Override
            List<Processor> getFilteredComponents(String name) {
                return dao.getProcessorsByMemoryType(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableMemoryTypes();
            }
        });
    }

    protected void initMenuButtonProducer() {
        initMenuButton(menuButtonProducer, processorsFilteredByProducer, new InitMenuButtonHelper() {
            @Override
            List<Processor> getFilteredComponents(String name) {
                return dao.getByProducer(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableProducers();
            }
        });
    }

}
