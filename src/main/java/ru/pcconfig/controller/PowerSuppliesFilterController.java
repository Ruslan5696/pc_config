package ru.pcconfig.controller;

import javafx.fxml.FXML;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;
import ru.pcconfig.controller.utils.EmptyFieldException;
import ru.pcconfig.controller.utils.FilteredComponentList;
import ru.pcconfig.controller.utils.Util;
import ru.pcconfig.model.dao.PowerSuppliesDAO;
import ru.pcconfig.model.dto.PowerSupplies;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Ruslan on 13.04.2017.
 */
public class PowerSuppliesFilterController extends ComponentFiltersController<PowerSupplies, PowerSuppliesDAO> {
    protected static final String MIX_POWER_ERROR = "������ ��� ������ ���� ��������";
    @FXML
    MenuButton menuButtonProducer;
    @FXML
    MenuButton menuButtonMotherboardPins;
    @FXML
    MenuButton menuButtonVideoCardPins;
    @FXML
    TextField textFieldMinPower;

    private FilteredComponentList<PowerSupplies> powerSuppliesFilteredByMotherboardPins = new FilteredComponentList();
    private FilteredComponentList<PowerSupplies> powerSuppliesFilteredByProducer = new FilteredComponentList();
    private FilteredComponentList<PowerSupplies> powerSuppliesFilteredByVideoCardPins = new FilteredComponentList();
    private FilteredComponentList<PowerSupplies> powerSuppliesFilteredByPower = new FilteredComponentList();

    @Override
    protected void readControls() {
        readTextFieldMinPower();
    }

    @PostConstruct
    private void postConstruct() {
        initMenuButtonVideoCardPins();
        initMenuButtonProducer();
        initMenuButtonMotherboardPins();
        currentComponentList = dao.getAll();
        updateViewList();

    }

    protected void readTextFieldMinPower() {
        try {
            int minPower = Util.readIntFormTextField(textFieldMinPower);
            Util.throwExIfLessOrEq(minPower, 0);
            powerSuppliesFilteredByPower.setComponents(dao.getPowerSuppliesByPower(minPower));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIX_POWER_ERROR);
        } catch (EmptyFieldException e) {
            powerSuppliesFilteredByPower.resetComponents();
        }
    }

    protected void initMenuButtonProducer() {
        initMenuButton(menuButtonProducer, powerSuppliesFilteredByProducer, new InitMenuButtonHelper() {
            @Override
            List<PowerSupplies> getFilteredComponents(String name) {
                return dao.getByProducer(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableProducers();
            }
        });
    }

    protected void initMenuButtonMotherboardPins() {
        initMenuButton(menuButtonMotherboardPins, powerSuppliesFilteredByMotherboardPins, new InitMenuButtonHelper() {
            @Override
            List<PowerSupplies> getFilteredComponents(String name) {
                return dao.getPowerSuppliesMotherboardPins(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableMotherboardPins();
            }
        });
    }

    protected void initMenuButtonVideoCardPins() {
        initMenuButton(menuButtonVideoCardPins, powerSuppliesFilteredByVideoCardPins, new InitMenuButtonHelper() {
            @Override
            List<PowerSupplies> getFilteredComponents(String name) {
                return dao.getPowerSuppliesVideoCardPins(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableVideoCardPins();
            }
        });
    }
}
