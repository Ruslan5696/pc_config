package ru.pcconfig.controller;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ru.pcconfig.controller.utils.Util;
import ru.pcconfig.model.priceParse.Price;
import ru.pcconfig.model.priceParse.PriceFinder;
import ru.pcconfig.model.dto.AbstractPcComponent;

import javax.annotation.PostConstruct;


/**
 * Created by Ruslan on 17.04.2017.
 */
public class PriceChooseController extends Controller {

    @FXML
    TableView table;
    @FXML
    Button buttonOk;
    @FXML
    Button buttonCancel;
    @FXML
    TableColumn columnShopName;
    @FXML
    TableColumn columnPrice;
    private AbstractPcComponent currentPcComponent;
    private ComponentController componentController;
    private Stage priceStage;

    @PostConstruct
    private void init() {
        initButtonOk();
        initButtonCancel();
        initTable();

    }
    private void initTable(){
        initColumns();
        table.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                Util.openWebpage(((Price) table.getSelectionModel().getSelectedItem()).getHref());
            }
        });
    }
    private void initColumns() {
        columnPrice.setCellValueFactory(new PropertyValueFactory<Price, Integer>("price"));
        columnShopName.setCellValueFactory(new PropertyValueFactory<Price, String>("shopName"));
    }

    private void initButtonOk() {
        buttonOk.setOnMouseClicked(event -> {
            currentPcComponent.setPrice((Price) table.getSelectionModel().getSelectedItem());
            componentController.setCurrentComponentDescription(currentPcComponent);
            priceStage.close();
        });
    }

    private void initButtonCancel() {
        buttonCancel.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                priceStage.close();
            }
        });
    }

    public TableView getTable() {
        return table;
    }

    public void setTable(TableView table) {
        this.table = table;
    }


    public void setComponentController(ComponentController componentController) {
        this.componentController = componentController;
    }

    public void setPcComponent(AbstractPcComponent currentPcComponent) {
        this.currentPcComponent = currentPcComponent;
        table.setItems(FXCollections.observableArrayList(PriceFinder.getComponentPrice(currentPcComponent.getName())));
    }

    public void showChooseComponentDialog() {
        priceStage.showAndWait();
    }

    public void initStage(Stage priceStage, Stage owner) {
        this.priceStage = priceStage;
        priceStage.setMinHeight(300);
        priceStage.setMinWidth(300);
        priceStage.setScene(new Scene(getRoot()));
        priceStage.initModality(Modality.WINDOW_MODAL);
        priceStage.initOwner(owner);

        priceStage.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                table.getItems().clear();
            }
        });
    }
}
