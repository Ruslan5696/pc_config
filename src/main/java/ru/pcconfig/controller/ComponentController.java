package ru.pcconfig.controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.pcconfig.controller.utils.Util;
import ru.pcconfig.model.dto.AbstractPcComponent;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;


/**
 * Created by Ruslan on 25.03.2017.
 */
public class ComponentController extends Controller {


    private MainController mainController;

    private ComponentFiltersController componentFiltersController;

    @Autowired
    ApplicationContext context;

    private Stage priceChooseStage;
    private Stage updateStage;
    private Stage componentStage;
    @Autowired
    PriceChooseController priceChooseController;
    @Autowired
    UpdateController updateController;
    private AbstractPcComponent currentPcComponent;
    @FXML
    private Button buttonApply;

    @FXML
    private Button buttonReset;

    @FXML
    private AnchorPane anchorPaneDescription;

    @FXML
    private VBox vBoxDescription;

    @FXML
    private VBox vBoxFilter;

    @FXML
    private ListView listViewComponent;

    @FXML
    private Button buttonClose;

    @FXML
    private Button buttonOk;
    @FXML
    private Button buttonFindPrices;

    @FXML
    private ScrollPane scrollPaneFilter;

    @FXML
    private Button buttonUpdate;


    @PostConstruct
    public void postConstruct() {
        initButtons();
        initListViewComponent();
    }

    private void initButtons() {
        initButtonApply();
        initButtonReset();
        initButtonClose();
        initOkButton();
        initFindPriceButton();
        initUpdateButton();

    }

    private void initUpdateButton() {
        buttonUpdate.setOnMouseClicked(event -> showUpdateDialog());


    }

    private void initOkButton() {
        buttonOk.setOnMouseClicked(event -> {
            mainController.setPcComponent(currentPcComponent);
            mainController.getComponentStage().close();

        });
    }

    private void initFindPriceButton() {

        buttonFindPrices.setOnMouseClicked(event -> showChoosePriceDialog());

    }

    private void initListViewComponent() {
        listViewComponent.setOnMouseClicked(event -> {
            if (event.getClickCount() == 1) {
                AbstractPcComponent pcComponent = (AbstractPcComponent) listViewComponent.getSelectionModel().getSelectedItem();
                setParamsToCurrentPcComponent(pcComponent);
            } else if (event.getClickCount() == 2) {
                //Action
            }
        });
    }

    private void initButtonApply() {
        buttonApply.setOnMouseClicked(event -> componentFiltersController.apply());
    }

    private void initButtonReset() {
        buttonReset.setOnMouseClicked(event -> componentFiltersController.reset());
    }

    private void initButtonClose() {
        buttonClose.setOnMouseClicked(event -> mainController.getComponentStage().close());

    }

    public void setCurrentComponentDescription(AbstractPcComponent pcComponent) {
        vBoxDescription.getChildren().clear();
        List<Label> description = Util.createDescriptionLabels(pcComponent, pcComponent.getParametersForFullDescription());
        Util.setFullDescriptionStyle(description);
        for (Label label : description) {
            vBoxDescription.getChildren().add(label);
        }

    }


    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }


    public void initialize() {

    }

    protected void setComponentFilter(ComponentFiltersController componentFiltersController) {
        this.componentFiltersController = componentFiltersController;
        scrollPaneFilter.setContent(componentFiltersController.getRoot());
    }

    public void showChooseComponentDialog() {
        componentStage.showAndWait();
    }

    public void componentStageInit(Stage componentStage, Stage owner) {
        this.componentStage = componentStage;
        componentStage.setMinHeight(400);
        componentStage.setMinWidth(900);
        componentStage.setScene(new Scene(getRoot()));
        componentStage.initModality(Modality.WINDOW_MODAL);
        componentStage.initOwner(owner);

        componentStage.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                vBoxDescription.getChildren().clear();
                scrollPaneFilter.setContent(null);
            }
        });
    }


    private void showUpdateDialog() {
        if (updateStage == null) {
            createUpdateStage();
        }
        updateController.setComponentController(this);
        updateController.setPcComponentClass(currentPcComponent.getClass());
        updateController.showChooseComponentDialog();

    }

    private void createUpdateStage() {
        updateStage = new Stage();
        updateController.initStage(updateStage, componentStage);

    }

    private void showChoosePriceDialog() {
        if (priceChooseStage == null)
            createPriceStage();
        priceChooseController.setComponentController(this);
        priceChooseController.setPcComponent(currentPcComponent);
        priceChooseController.showChooseComponentDialog();
    }

    private void createPriceStage() {
        priceChooseStage = new Stage();
        priceChooseController.initStage(priceChooseStage, componentStage);
    }


    public ListView getListViewComponent() {
        return listViewComponent;
    }

    public void setListViewComponent(ListView listViewComponent) {
        this.listViewComponent = listViewComponent;
    }

    public AbstractPcComponent getCurrentPcComponent() {
        return currentPcComponent;
    }

    private void setParamsToCurrentPcComponent(AbstractPcComponent currentPcComponent) {
        this.currentPcComponent.setAllParams(currentPcComponent);
        setCurrentComponentDescription(this.currentPcComponent);
        buttonFindPrices.setDisable(false);
    }

    public void setCurrentPcComponent(AbstractPcComponent currentPcComponent) {

        this.currentPcComponent = currentPcComponent;
        if (currentPcComponent.getName()!=null)
            buttonFindPrices.setDisable(false);
        setCurrentComponentDescription(this.currentPcComponent);
    }

    public Stage getPriceChooseStage() {
        return priceChooseStage;
    }

    public <T extends AbstractPcComponent> void updateViewList(List<T> currentComponentList) {
        getListViewComponent().getItems().clear();
        getListViewComponent().getItems().addAll(currentComponentList);
    }
}
