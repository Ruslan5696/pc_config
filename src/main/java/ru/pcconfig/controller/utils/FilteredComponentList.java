package ru.pcconfig.controller.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Ruslan on 11.04.2017.
 */
public class FilteredComponentList<T> extends ArrayList<T> {
    private boolean isReset = true;



    public boolean isReset() {
        return isReset;
    }

    public void resetComponents() {
        this.clear();
        isReset = true;
    }


    public Object setComponents(Collection<T> components) {
        resetComponents();
        isReset = false;
        return super.addAll(components);
    }
}
