package ru.pcconfig.controller.utils;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import ru.pcconfig.model.priceParse.Price;
import ru.pcconfig.model.dto.AbstractPcComponent;

import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Ruslan on 25.03.2017.
 */
public class Util {
    public static final String NOT_SELECTED = "�� ��������";
    public static final String PRICE = "����";
    public static final String SHOP = "�������";
    public static List<Label> createDescriptionLabels(AbstractPcComponent pcComponent, Map<String, String> paramsForView){
        List<Label> labels = new ArrayList<>();
        String titleText = pcComponent.getName();
        Label title = new Label(titleText);
        labels.add(title);
        if (titleText==null){
            title.setText(NOT_SELECTED);
            return labels;
        }
        Field[] fields = pcComponent.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                if (field.get(pcComponent)!=null)
                    if (paramsForView.containsKey(field.getName())){
                        Label label = new Label(paramsForView.get(field.getName())+": "+field.get(pcComponent));
                        labels.add(label);
                    }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        Price price;
        if ((price = pcComponent.getPrice())!=null){
            Label priceLabel = new Label(PRICE+": "+price.getPrice());
            Label shopLabel = new Label(SHOP+": "+price.getShopName());
            labels.add(priceLabel);
            labels.add(shopLabel);
        }
        return labels;
    }


    public static List<Label> setFullDescriptionStyle(List<Label> labels){
        if (labels.size()>0){
            labels.get(0).setStyle("-fx-font-size: 14px");
            labels.get(0).setPadding(new Insets(5));;
            for (int i = 1; i < labels.size(); i++) {
                labels.get(i).setPadding(new Insets(0,0,0,15));
            }
        }
        return labels;
    }

    public static List<Label> setShortDescriptionStyle(List<Label> labels){
        if (labels.size()>0){
            labels.get(0).setStyle("-fx-font-size: 20px");
            labels.get(0).setPadding(new Insets(5));;
            for (int i = 1; i < labels.size(); i++) {
                labels.get(i).setTextFill(Color.web("#7c7c7c"));
                labels.get(i).setPadding(new Insets(5,0,0,15));
            }
        }
        return labels;
    }

    public static Label createWarningLabel(final String warningName, final String warningDesc) {
        final Label label = new Label(warningName);
        setWarningLabelStyle(label);
        label.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                showInfoDialog(warningName, warningDesc);
            }
        });
        return label;
    }

    public static void setWarningLabelStyle(final Label label) {
        label.setTextFill(Color.web("#CD5C5C"));
        label.setOnMouseEntered(event -> label.setTextFill(Color.web("#8B0000")));

        label.setOnMouseExited(event -> label.setTextFill(Color.web("#CD5C5C")));
    }

    public static Label createLinkLabel(String url){
        final Label label = new Label(url);
        setLinkLabelStyle(label);
        label.setOnMouseClicked(event -> openWebpage(url));
        return label;
    }

    public static void setLinkLabelStyle(Label label){
        label.setTextFill(Color.web("#00C0FF"));
        label.setOnMouseEntered(event -> label.setTextFill(Color.web("#0037FF")));

        label.setOnMouseExited(event -> label.setTextFill(Color.web("#00C0FF")));

    }

    public static void openWebpage(String url) {
        try {
            Desktop.getDesktop().browse(new URI(url));
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (URISyntaxException e1) {
            e1.printStackTrace();
        }

    }

    public static int readIntFormTextField(TextField textField) {
        String value = textField.getText();
        if (!value.equals("")) {
            return Integer.parseInt(value);
        } else throw new EmptyFieldException();
    }

    public static double readDoubleFormTextField(TextField textField) {
        String value = textField.getText();
        if (!value.equals("")) {
            value = value.replaceAll(",", ".");
            return Double.parseDouble(value);
        } else throw new EmptyFieldException();
    }

    public static void throwExIfLessOrEq(double value, double basic){
        if (value <= basic) {
            throw new NumberFormatException();
        }
    }

    public static void showErrorDialog(String title, String text) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        fillAndShowAlert(alert, title, text);
    }

    public static void showInfoDialog(String title, String text) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        fillAndShowAlert(alert, title, text);
    }

    private static void fillAndShowAlert(Alert alert, String title, String text){
        alert.setContentText(text);
        alert.setTitle(title);
        alert.setHeaderText("");
        alert.showAndWait();
    }



}
