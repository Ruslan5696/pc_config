package ru.pcconfig.controller;

import javafx.fxml.FXML;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pcconfig.controller.utils.EmptyFieldException;
import ru.pcconfig.controller.utils.FilteredComponentList;
import ru.pcconfig.controller.utils.Util;

import ru.pcconfig.model.dao.HddDAO;
import ru.pcconfig.model.dto.Hdd;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Ruslan on 13.04.2017.
 */
public class HddFiltersController extends ComponentFiltersController<Hdd, HddDAO> {


    private static final String MIN_HDD_SIZE_ERROR = "������ ������ ������ �������� �����";

    @Autowired
    HddDAO hddDAO;

    @FXML
    MenuButton menuButtonProducer;

    @FXML
    MenuButton menuButtonFormFactor;

    @FXML
    MenuButton menuButtonInterface;

    @FXML
    TextField textFieldSize;

    @PostConstruct
    private void postConstruct() {
        initMenuButtonFormFactor();
        initMenuButtonProducer();
        initMenuButtonInterface();

        currentComponentList = hddDAO.getAll();
        updateViewList();

    }

    private FilteredComponentList<Hdd> hddsFilteredByInterface = new FilteredComponentList<>();
    private FilteredComponentList<Hdd> hddsFilteredByProducer = new FilteredComponentList<>();
    private FilteredComponentList<Hdd> hddsFilteredByFormFactor = new FilteredComponentList<>();
    private FilteredComponentList<Hdd> hddsFilteredBySize = new FilteredComponentList<>();

    protected void readControls(){
        readTextFieldMinDdr3Amount();
    }

    protected void readTextFieldMinDdr3Amount() {
        try {
            int hddSize = Util.readIntFormTextField(textFieldSize);
            Util.throwExIfLessOrEq(hddSize, 0);
            hddsFilteredBySize.setComponents(hddDAO.getHddsByMemorySize(hddSize));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_HDD_SIZE_ERROR);
        } catch (EmptyFieldException e) {
            hddsFilteredBySize.resetComponents();
        }
    }

    private void initMenuButtonInterface() {
        initMenuButton(menuButtonInterface, hddsFilteredByInterface, new InitMenuButtonHelper() {
            @Override
            List<Hdd> getFilteredComponents(String name) {
                return hddDAO.getHddsByInterfaceConnection(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return hddDAO.getAvailableInterfaceConnection();
            }
        });
    }

    private void initMenuButtonProducer() {
        initMenuButton(menuButtonProducer, hddsFilteredByProducer, new InitMenuButtonHelper() {
            @Override
            List<Hdd> getFilteredComponents(String name) {
                return hddDAO.getByProducer(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return hddDAO.getAvailableProducers();
            }
        });
    }

    private void initMenuButtonFormFactor() {
        initMenuButton(menuButtonFormFactor, hddsFilteredByFormFactor, new InitMenuButtonHelper() {
            @Override
            List<Hdd> getFilteredComponents(String name) {
                return hddDAO.getHddsByFormFactor(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return hddDAO.getAvailableFormFactors();
            }
        });
    }


}
