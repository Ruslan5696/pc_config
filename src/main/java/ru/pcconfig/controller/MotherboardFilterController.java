package ru.pcconfig.controller;

import javafx.fxml.FXML;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;
import ru.pcconfig.controller.utils.EmptyFieldException;
import ru.pcconfig.controller.utils.FilteredComponentList;
import ru.pcconfig.controller.utils.Util;
import ru.pcconfig.model.dao.MotherboardDAO;
import ru.pcconfig.model.dto.Motherboard;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Ruslan on 11.04.2017.
 */
public class MotherboardFilterController extends ComponentFiltersController<Motherboard, MotherboardDAO> {

    private static final String MIN_DDR3_AMOUNT_ERROR = "������ ������ ���������� ������ DDR3";
    private static final String MIN_DDR4_AMOUNT_ERROR = "������ ������ ���������� ������ DDR4";
    private static final String MIN_PCI_AMOUNT_ERROR = "������ ������ ���������� ������ PCI";
    private static final String MIN_PCI20_AMOUNT_ERROR = "������ ������ ���������� ������ PCI 2.0 x16";
    private static final String MIN_PCI30_AMOUNT_ERROR = "������ ������ ���������� ������ PCI 3.0 x16";
    private static final String MIN_SATA2_AMOUNT_ERROR = "������ ������ ���������� �������� SATA 2";
    private static final String MIN_SATA3_AMOUNT_ERROR = "������ ������ ���������� �������� SATA 3";



    @FXML
    MenuButton menuButtonProducer;

    @FXML
    MenuButton menuButtonSocket;

    @FXML
    MenuButton menuButtonFormFactor;

    @FXML
    MenuButton menuButtonPowerPins;

    @FXML
    TextField textFieldMinDdr3Amount;

    @FXML
    TextField textFieldMinDdr4Amount;

    @FXML
    TextField textFieldMinPciAmount;

    @FXML
    TextField textFieldMinSata2Amount;

    @FXML
    TextField textFieldMinPci20x16Amount;

    @FXML
    TextField textFieldMinPci30x16Amount;

    @FXML
    TextField textFieldMinSata3Amount;

    private FilteredComponentList<Motherboard> motherboardsFilteredBySocket = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByProducer = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByPowerPins = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByFormFactor = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByMinDdr3Amount = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByMinDdr4Amount = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByMinPciAmount = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByMinPci20x16Amount = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByMinPci30x16Amount = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByMinSata2Amount = new FilteredComponentList<>();
    private FilteredComponentList<Motherboard> motherboardsFilteredByMinSata3Amount = new FilteredComponentList<>();

    @PostConstruct
    private void postConstruct() {
        initMenuButtonSocket();
        initMenuButtonProducer();
        initMenuButtonFormFactor();
        initMenuButtonPowerPins();
        currentComponentList = dao.getAll();
        updateViewList();

    }

    protected void initMenuButtonSocket() {
        initMenuButton(menuButtonSocket, motherboardsFilteredBySocket, new InitMenuButtonHelper() {
            @Override
            List<Motherboard> getFilteredComponents(String name) {
                return dao.getMotherboardsBySocket(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableSockets();
            }
        });
    }

    protected void initMenuButtonProducer() {
        initMenuButton(menuButtonProducer, motherboardsFilteredByProducer, new InitMenuButtonHelper() {
            @Override
            List<Motherboard> getFilteredComponents(String name) {
                return dao.getByProducer(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableProducers();
            }
        });
    }

    protected void initMenuButtonFormFactor() {
        initMenuButton(menuButtonFormFactor, motherboardsFilteredByFormFactor, new InitMenuButtonHelper() {
            @Override
            List<Motherboard> getFilteredComponents(String name) {
                return dao.getMotherboardsByFormFactor(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableFormFactors();
            }
        });
    }

    protected void readControls(){
        readTextFieldMinDdr3Amount();
        readTextFieldMinDdr4Amount();
        readTextFieldMinPci20x16AmountAmount();
        readTextFieldMinPci30x16AmountAmount();
        readTextFieldMinSata2AmountAmount();
        readTextFieldMinSata3AmountAmount();
    }

    protected void readTextFieldMinDdr3Amount() {
        try {
            int ddr3Amount = Util.readIntFormTextField(textFieldMinDdr3Amount);
            Util.throwExIfLessOrEq(ddr3Amount, 0);
            motherboardsFilteredByMinDdr3Amount.setComponents(dao.getMotherboardsByDdr3Amount(ddr3Amount));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_DDR3_AMOUNT_ERROR);
        } catch (EmptyFieldException e) {
            motherboardsFilteredByMinDdr3Amount.resetComponents();
        }
    }

    protected void readTextFieldMinDdr4Amount() {
        try {
            int ddr4Amount = Util.readIntFormTextField(textFieldMinDdr4Amount);
            Util.throwExIfLessOrEq(ddr4Amount, 0);
            motherboardsFilteredByMinDdr4Amount.setComponents(dao.getMotherboardsByDdr4Amount(ddr4Amount));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_DDR4_AMOUNT_ERROR);
        } catch (EmptyFieldException e) {
            motherboardsFilteredByMinDdr4Amount.resetComponents();
        }
    }

    protected void readTextFieldMinPciAmountAmount() {
        try {
            int pciAmount = Util.readIntFormTextField(textFieldMinPciAmount);
            Util.throwExIfLessOrEq(pciAmount, 0);
            motherboardsFilteredByMinPciAmount.setComponents(dao.getMotherboardsByPciAmount(pciAmount));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_PCI_AMOUNT_ERROR);
        } catch (EmptyFieldException e) {
            motherboardsFilteredByMinPciAmount.resetComponents();
        }
    }

    protected void readTextFieldMinPci20x16AmountAmount() {
        try {
            int pci20x16Amount = Util.readIntFormTextField(textFieldMinPci20x16Amount);
            Util.throwExIfLessOrEq(pci20x16Amount, 0);
            motherboardsFilteredByMinPci20x16Amount.setComponents(dao.getMotherboardsByPciE20x16Amount(pci20x16Amount));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_PCI20_AMOUNT_ERROR);
        } catch (EmptyFieldException e) {
            motherboardsFilteredByMinPci20x16Amount.resetComponents();
        }
    }

    protected void readTextFieldMinPci30x16AmountAmount() {
        try {
            int pci30x16Amount = Util.readIntFormTextField(textFieldMinPci30x16Amount);
            Util.throwExIfLessOrEq(pci30x16Amount, 0);
            motherboardsFilteredByMinPci30x16Amount.setComponents(dao.getMotherboardsByPciE30x16Amount(pci30x16Amount));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_PCI30_AMOUNT_ERROR);
        } catch (EmptyFieldException e) {
            motherboardsFilteredByMinPci30x16Amount.resetComponents();
        }
    }

    protected void readTextFieldMinSata2AmountAmount() {
        try {
            int sata2Amount = Util.readIntFormTextField(textFieldMinSata2Amount);
            Util.throwExIfLessOrEq(sata2Amount, 0);
            motherboardsFilteredByMinSata2Amount.setComponents(dao.getMotherboardsBySata2Amount(sata2Amount));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_SATA2_AMOUNT_ERROR);
        } catch (EmptyFieldException e) {
            motherboardsFilteredByMinSata2Amount.resetComponents();
        }
    }

    protected void readTextFieldMinSata3AmountAmount() {
        try {
            int sata3Amount = Util.readIntFormTextField(textFieldMinSata3Amount);
            Util.throwExIfLessOrEq(sata3Amount, 0);
            motherboardsFilteredByMinSata3Amount.setComponents(dao.getMotherboardsBySata3Amount(sata3Amount));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_SATA3_AMOUNT_ERROR);
        } catch (EmptyFieldException e) {
            motherboardsFilteredByMinSata3Amount.resetComponents();
        }
    }

    protected void initMenuButtonPowerPins() {
        initMenuButton(menuButtonPowerPins, motherboardsFilteredByPowerPins, new InitMenuButtonHelper() {
            @Override
            List<Motherboard> getFilteredComponents(String name) {
                return dao.getMotherboardsByPowerPins(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailablePowerPins();
            }
        });
    }



}
