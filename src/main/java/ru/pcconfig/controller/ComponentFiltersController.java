package ru.pcconfig.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pcconfig.model.dao.ComponentDAO;
import ru.pcconfig.model.dto.AbstractPcComponent;
import ru.pcconfig.controller.utils.FilteredComponentList;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruslan on 06.04.2017.
 */
public abstract class ComponentFiltersController<T extends AbstractPcComponent, D extends ComponentDAO> extends Controller {
    @Autowired
    protected ComponentController componentController;

    protected static final String NOT_SELECTED = "�� �������";
    protected static final String ERROR = "������";

    protected List<T> currentComponentList;

    @Autowired
    protected D dao;


    protected void updateViewList() {
        componentController.updateViewList(currentComponentList);

    }


    protected MenuItem createNotSelectedMenuItem(final FilteredComponentList<T> filteredComponent, final MenuButton menuButton) {
        final MenuItem notSelectedItem = new MenuItem(NOT_SELECTED);
        notSelectedItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (event.getSource().equals(menuButton.getItems().get(0))) {
                    menuButton.setText(NOT_SELECTED);
                    filteredComponent.resetComponents();
                }
            }
        });
        return notSelectedItem;
    }


    protected void initMenuButton(final MenuButton menuButton, final FilteredComponentList<T> filteredComponentList, final InitMenuButtonHelper initMenuButtonHelper) {
        //final MenuButton menuButton = new MenuButton();
        menuButton.setText(NOT_SELECTED);
        List<MenuItem> menuItems = new ArrayList<>();
        menuItems.add(createNotSelectedMenuItem(filteredComponentList, menuButton));
        List<String> menuItemsNames = initMenuButtonHelper.getMenuItemNames();
        for (final String menuItemsName : menuItemsNames) {
            final MenuItem menuItem = new MenuItem(menuItemsName);
            menuItem.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    menuButton.setText(menuItemsName);
                    filteredComponentList.setComponents(initMenuButtonHelper.getFilteredComponents(menuItemsName));
                }
            });
            menuItems.add(menuItem);
        }
        MenuItem[] items = new MenuItem[0];
        items = menuItems.toArray(items);
        menuButton.getItems().addAll(items);
        //return menuButton;
    }

    protected abstract class InitMenuButtonHelper {
        abstract List<T> getFilteredComponents(String name);
        abstract List<String> getMenuItemNames();
    }


    protected void applyFilters() {
        for (Field field : this.getClass().getDeclaredFields()) {
            if (field.getType().equals(FilteredComponentList.class)) {
                field.setAccessible(true);
                try {
                    FilteredComponentList filteredComponentList = ((FilteredComponentList) field.get(this));
                    if (!filteredComponentList.isReset())
                        currentComponentList.retainAll(filteredComponentList);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    protected void resetFilteredComponents() {
        for (Field field : this.getClass().getDeclaredFields()) {
            if (field.getType().equals(FilteredComponentList.class)) {
                field.setAccessible(true);
                try {
                    ((FilteredComponentList) field.get(this)).resetComponents();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void resetTextFields() {
        for (Field field : this.getClass().getDeclaredFields()) {
            if (field.getType().equals(TextField.class)) {
                field.setAccessible(true);
                try {
                    ((TextField) field.get(this)).setText("");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void resetMenuButtons() {
        for (Field field : this.getClass().getDeclaredFields()) {
            if (field.getType().equals(MenuButton.class)) {
                field.setAccessible(true);
                try {
                    ((MenuButton) field.get(this)).setText(NOT_SELECTED);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void resetCheckBox() {
        for (Field field : this.getClass().getDeclaredFields()) {
            if (field.getType().equals(CheckBox.class)) {
                field.setAccessible(true);
                try {
                    ((CheckBox) field.get(this)).setSelected(false);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    protected void reset() {
        resetFilteredComponents();
        resetMenuButtons();
        resetCheckBox();
        resetTextFields();
        currentComponentList = dao.getAll();
        updateViewList();
    }

    protected void apply() {
        readControls();
        applyFilters();
        updateViewList();
        currentComponentList = dao.getAll();
    }


    protected abstract void readControls();






}
