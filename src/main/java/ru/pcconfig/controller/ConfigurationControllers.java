package ru.pcconfig.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Ruslan on 30.03.2017.
 */
@Configuration
public class ConfigurationControllers  {


    @Bean
    public Controller componentController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/componentView.fxml");
        return createController(fxmlStream);
    }

    @Bean
    public Controller mainController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/main.fxml");
        //MainController mainController = (MainController)createController(fxmlStream);
        return createController(fxmlStream);

    }

    @Bean
    @Scope("prototype")
    public Controller processorFilterController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/processor_filter.fxml");
        //MainController mainController = (MainController)createController(fxmlStream);
        return createController(fxmlStream);

    }

    @Bean
    @Scope("prototype")
    public Controller motherboardFilterController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/motherboard_filter.fxml");
        return createController(fxmlStream);

    }

    @Bean
    @Scope("prototype")
    public Controller hddFilterController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/hdd_filter.fxml");
        return createController(fxmlStream);

    }

    @Bean
    @Scope("prototype")
    public Controller videoCardFilterController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/video_card_filter.fxml");
        return createController(fxmlStream);

    }

    @Bean
    @Scope("prototype")
    public Controller ramFilterController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/ram_filte.fxml");
        return createController(fxmlStream);

    }

    @Bean
    @Scope("prototype")
    public Controller powerSuppliesFilterController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/power_supplies_filter.fxml");
        return createController(fxmlStream);

    }

    @Bean
    @Scope("prototype")
    public PriceChooseController priceChooseController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/price_choose_menu.fxml");
        return (PriceChooseController)createController(fxmlStream);

    }

    @Bean
    @Scope("prototype")
    public UpdateController updateController(){
        InputStream fxmlStream = getClass().getClassLoader().getResourceAsStream("view/update_view.fxml");
        return (UpdateController)createController(fxmlStream);

    }



    private Controller createController(InputStream resource) {
        FXMLLoader loader = new FXMLLoader();
        try {
            Parent root = loader.load(resource);
            Controller controller = loader.getController();
            controller.setRoot(root);
            return controller;
        } catch (IOException e) {

            throw new RuntimeException(e);
        }
    }
}
