package ru.pcconfig.controller;

import javafx.scene.Parent;

/**
 * Created by Ruslan on 30.03.2017.
 */
public abstract class Controller {
    Parent root;

    public Parent getRoot() {
        return root;
    }

    public void setRoot(Parent root) {
        this.root = root;
    }
}
