package ru.pcconfig.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.jsoup.HttpStatusException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.pcconfig.controller.utils.Util;
import ru.pcconfig.model.ApplicationContextProvider;
import ru.pcconfig.model.dao.DAOImpl.AbstractComponentDAO;
import ru.pcconfig.model.componentCreatorByRef.ComponentCreator;
import ru.pcconfig.model.componentCreatorByRef.ComponentFinder;
import ru.pcconfig.model.dto.AbstractPcComponent;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Ruslan on 05.05.2017.
 */
public class UpdateController extends Controller {
//    @Autowired
//    ApplicationContext context;
    private Class pcComponentClass;
    private AbstractComponentDAO<AbstractPcComponent> componentDAO;
    private ComponentFinder componentFinder;
    private ComponentCreator<AbstractPcComponent> componentCreator;
    private Thread updateThread;
    private Object lock = new Object();
    private ComponentController componentController;
    @FXML
    private Label labelUpdateStatus;


    Stage stage;
    @FXML
    ProgressIndicator progressIndicator;
    @FXML
    Button buttonCancel;
    private boolean stopUpdate;

    @PostConstruct
    public void PostConstruct() {
        Logger.getGlobal().setLevel(Level.FINEST);
        Logger.getGlobal().log(Level.FINE, "init");

        initCancelButton();
    }

    private void initCancelButton() {
        buttonCancel.setOnAction(value -> {

            stage.close();
        });


    }

    public void initStage(Stage stage, Stage owner) {
        this.stage = stage;
        stage.setMinHeight(120);
        stage.setMinWidth(381);
        stage.setScene(new Scene(getRoot()));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(owner);
        stage.setOnHiding(value -> stopUpdate = true);
    }

    public void showChooseComponentDialog() {
        stopUpdate = false;
        progressIndicator.setProgress(-1.0);
        labelUpdateStatus.setText("���� ����������");
        updateThread = new Thread(() -> updateComponents());
        updateThread.setDaemon(true);
        updateThread.start();
        stage.showAndWait();

    }

    private void showOverflowRequestAlert(String url) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Look, an Information Dialog");
        alert.setContentText("I have a great message for you!");
        DialogPane dialogPane = new DialogPane();
        dialogPane.setMinSize(300, 300);
        Label message = new Label("� �������� ���������� ������ �����������" +
                "\n���� ��������� ���������� ��������� � �����." +
                "\n���������� ������� �� ������ � ������ ��� � ��������");
        Label link = Util.createLinkLabel(url);
        VBox vBox = new VBox(10);
        vBox.setMaxWidth(Double.MAX_VALUE);
        vBox.setMaxHeight(Double.MAX_VALUE);
        vBox.setVisible(true);
        vBox.getChildren().addAll(message, link);
        dialogPane.setContent(vBox);
        dialogPane.getButtonTypes().addAll(ButtonType.OK);

        alert.setDialogPane(dialogPane);
        alert.setOnHiding((event) -> {
            synchronized (lock) {
                lock.notify();
            }
        });
        alert.showAndWait();

    }

    public void waitForOverflowRequestAlert() {
        synchronized (lock) {
            try {
                lock.wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void updateComponents() {
        StringBuilder existingComponents = new StringBuilder();
        for (AbstractPcComponent abstractPcComponent : componentDAO.getAll()) {
            //Logger.getGlobal().info(abstractPcComponent.toString());
            existingComponents.append(abstractPcComponent.getName());
        }
        Logger.getGlobal().fine("appended");
        List<String> refs = new ArrayList<>();
        System.out.println(existingComponents);
        int pageAmount = 0;
        while (true) {
            if (stopUpdate)
                break;
            try {
                pageAmount = componentFinder.findPageAmount();
                break;
            } catch (HttpStatusException e) {
                Platform.runLater(() -> showOverflowRequestAlert(e.getUrl()));
                waitForOverflowRequestAlert();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (int i = 1; i <= pageAmount; i++) {
            if (stopUpdate)
                return;
            try {
                refs.addAll(componentFinder.findNewRefsOnPage(componentFinder.getUrlByPageNumber(i), existingComponents.toString()));
            } catch (HttpStatusException e) {
                i--;
                Platform.runLater(() -> showOverflowRequestAlert(e.getUrl()));
                waitForOverflowRequestAlert();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < refs.size(); i++) {
            if (stopUpdate)
                return;
            componentCreator.setURL(refs.get(i));
            try {
                componentDAO.insert(componentCreator.create());
            } catch (HttpStatusException e) {
                e.printStackTrace();
                i--;
                Platform.runLater(() -> showOverflowRequestAlert(e.getUrl()));
                waitForOverflowRequestAlert();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Platform.runLater(() -> progressIndicator.setProgress(1.0));
        Platform.runLater(() -> labelUpdateStatus.setText("���������"));
        Platform.runLater(() -> componentController.updateViewList(componentDAO.getAll()));
        Platform.runLater(() -> buttonCancel.setText("�������"));
    }

    public void setPcComponentClass(Class pcComponentClass) {
        this.pcComponentClass = pcComponentClass;
        switch (pcComponentClass.getSimpleName()) {
            case "Hdd":
                componentDAO = (AbstractComponentDAO) ApplicationContextProvider.getApplicationContext().getBean("hddDAODerby");
                componentFinder = (ComponentFinder) ApplicationContextProvider.getApplicationContext().getBean("hddFinder");
                componentCreator = (ComponentCreator) ApplicationContextProvider.getApplicationContext().getBean("hddCreator");
                break;
            case "Motherboard":
                componentDAO = (AbstractComponentDAO) ApplicationContextProvider.getApplicationContext().getBean("motherboardDAODerby");
                componentFinder = (ComponentFinder) ApplicationContextProvider.getApplicationContext().getBean("motherboardFinder");
                componentCreator = (ComponentCreator) ApplicationContextProvider.getApplicationContext().getBean("motherboardCreator");
                break;
            case "PowerSupplies":
                componentDAO = (AbstractComponentDAO) ApplicationContextProvider.getApplicationContext().getBean("powerSuppliesDAODerby");
                componentFinder = (ComponentFinder) ApplicationContextProvider.getApplicationContext().getBean("powerSuppliesFinder");
                componentCreator = (ComponentCreator) ApplicationContextProvider.getApplicationContext().getBean("powerSuppliesCreator");
                break;
            case "Processor":
                componentDAO = (AbstractComponentDAO) ApplicationContextProvider.getApplicationContext().getBean("processorDAODerby");
                componentFinder = (ComponentFinder) ApplicationContextProvider.getApplicationContext().getBean("processorFinder");
                componentCreator = (ComponentCreator) ApplicationContextProvider.getApplicationContext().getBean("processorCreator");
                break;
            case "Ram":
                componentDAO = (AbstractComponentDAO) ApplicationContextProvider.getApplicationContext().getBean("ramDAODerby");
                componentFinder = (ComponentFinder) ApplicationContextProvider.getApplicationContext().getBean("ramFinder");
                componentCreator = (ComponentCreator) ApplicationContextProvider.getApplicationContext().getBean("ramCreator");
                break;
            case "VideoCard":
                componentDAO = (AbstractComponentDAO) ApplicationContextProvider.getApplicationContext().getBean("videoCardDAODerby");
                componentFinder = (ComponentFinder) ApplicationContextProvider.getApplicationContext().getBean("videoCardFinder");
                componentCreator = (ComponentCreator) ApplicationContextProvider.getApplicationContext().getBean("videoCardCreator");
                break;
        }
    }

    public void setComponentController(ComponentController componentController) {
        this.componentController = componentController;
    }
}
