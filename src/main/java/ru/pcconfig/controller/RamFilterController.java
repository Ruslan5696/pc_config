package ru.pcconfig.controller;

import javafx.fxml.FXML;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;
import ru.pcconfig.controller.utils.EmptyFieldException;
import ru.pcconfig.controller.utils.FilteredComponentList;
import ru.pcconfig.controller.utils.Util;
import ru.pcconfig.model.dao.RamDAO;
import ru.pcconfig.model.dto.Ram;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Ruslan on 13.04.2017.
 */
public class RamFilterController extends ComponentFiltersController<Ram, RamDAO> {


    protected static final String MIN_FREQ_ERROR = "������ ��� ������ ���� ����������� �������";
    protected static final String MIN_MODULES_AMOUNT_ERROR = "������ ��� ������ ���� ������������ ���������� �������";
    protected static final String MIN_MEMORY_ERROR = "������ ��� ������ ���� ������������ ������ ������";

    @FXML
    MenuButton menuButtonProducer;
    @FXML
    MenuButton menuButtonFormFactor;
    @FXML
    MenuButton menuButtonMemoryType;
    @FXML
    TextField textFieldMinMemorySize;
    @FXML
    TextField textFieldMinFreq;
    @FXML
    TextField textFieldModulesAmount;

    private FilteredComponentList<Ram> ramsFilteredByModulesAmount = new FilteredComponentList();
    private FilteredComponentList<Ram> ramsFilteredByProducer = new FilteredComponentList();
    private FilteredComponentList<Ram> ramsFilteredByMemorySize = new FilteredComponentList();
    private FilteredComponentList<Ram> ramsFilteredByMemoryType = new FilteredComponentList();
    private FilteredComponentList<Ram> ramsFilteredByFormFactor = new FilteredComponentList();
    private FilteredComponentList<Ram> ramsFilteredByMinFreq = new FilteredComponentList();


    @PostConstruct
    private void postConstruct() {
        initMenuButtonFormFactor();
        initMenuButtonProducer();
        initMenuButtonMemoryType();
        currentComponentList = dao.getAll();
        updateViewList();
    }

    protected void initMenuButtonProducer() {
        initMenuButton(menuButtonProducer, ramsFilteredByProducer, new InitMenuButtonHelper() {
            @Override
            List<Ram> getFilteredComponents(String name) {
                return dao.getByProducer(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableProducers();
            }
        });
    }

    protected void initMenuButtonFormFactor() {
        initMenuButton(menuButtonFormFactor, ramsFilteredByFormFactor, new InitMenuButtonHelper() {
            @Override
            List<Ram> getFilteredComponents(String name) {
                return dao.getRamByFormFactor(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableFormFactors();
            }
        });
    }

    protected void initMenuButtonMemoryType() {
        initMenuButton(menuButtonMemoryType, ramsFilteredByMemoryType, new InitMenuButtonHelper() {
            @Override
            List<Ram> getFilteredComponents(String name) {
                return dao.getRamByMemoryType(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableDdrTypes();
            }
        });
    }

    protected void readTextFieldFreq() {
        try {
            int minFreq = Util.readIntFormTextField(textFieldMinFreq);
            Util.throwExIfLessOrEq(minFreq, 0);
            ramsFilteredByMinFreq.setComponents(dao.getRamByFrequency(minFreq));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_FREQ_ERROR);
        } catch (EmptyFieldException e) {
            ramsFilteredByMinFreq.resetComponents();
        }
    }

    protected void readTextFieldMemorySize() {
        try {
            int minMemorySize = Util.readIntFormTextField(textFieldMinMemorySize);
            Util.throwExIfLessOrEq(minMemorySize, 0);
            ramsFilteredByMemorySize.setComponents(dao.getRamByMemorySize(minMemorySize));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_MEMORY_ERROR);
        } catch (EmptyFieldException e) {
            ramsFilteredByMinFreq.resetComponents();
        }
    }

    protected void readTextFieldModulesAmount() {
        try {
            int modulesAmount = Util.readIntFormTextField(textFieldModulesAmount);
            Util.throwExIfLessOrEq(modulesAmount, 0);
            ramsFilteredByModulesAmount.setComponents(dao.getRamByModulesAmount(modulesAmount));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_MODULES_AMOUNT_ERROR);
        } catch (EmptyFieldException e) {
            ramsFilteredByModulesAmount.resetComponents();
        }
    }


    @Override
    protected void readControls() {
        readTextFieldFreq();
        readTextFieldMemorySize();
        readTextFieldModulesAmount();
    }
}
