package ru.pcconfig.controller;

import javafx.fxml.FXML;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;
import ru.pcconfig.controller.utils.EmptyFieldException;
import ru.pcconfig.controller.utils.FilteredComponentList;
import ru.pcconfig.controller.utils.Util;
import ru.pcconfig.model.dao.VideoCardDAO;
import ru.pcconfig.model.dto.VideoCard;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * Created by Ruslan on 13.04.2017.
 */
public class VideoCardFiltersController extends ComponentFiltersController<VideoCard, VideoCardDAO> {
    public static void main(String[] args) {
        System.out.println(isPalindrome("Madam, I'm Adam!"));

    }
    public static boolean isPalindrome(String text) {
        text=text.replaceAll("[^a-zA-Z0-9]", "");
        String txet =new StringBuilder(text).reverse().toString() ;
        return  text.equalsIgnoreCase(txet);// your implementation here
    }
    protected static final String MIN_FREQ_ERROR = "������ ��� ������ ���� ����������� �������";
    protected static final String MIN_DEPTH_ERROR = "������ ��� ������ ���� ����������� ������ ����";
    protected static final String MIN_MEMORY_ERROR = "������ ��� ������ ���� ������������ ������ ������";


    @FXML
    MenuButton menuButtonProducer;

    @FXML
    MenuButton menuButtonChipset;

    @FXML
    MenuButton menuButtonPowerPins;

    @FXML
    MenuButton menuButtonInterface;

    @FXML
    TextField textFieldMinFreq;

    @FXML
    TextField textFieldMinBusDepth;

    @FXML
    TextField textFieldMinMemory;

    private FilteredComponentList<VideoCard> videoCardsFilteredByProducer = new FilteredComponentList();
    private FilteredComponentList<VideoCard> videoCardsFilteredByChipset = new FilteredComponentList();
    private FilteredComponentList<VideoCard> videoCardsFilteredByPowerPins = new FilteredComponentList();
    private FilteredComponentList<VideoCard> videoCardsFilteredByInterface = new FilteredComponentList();
    private FilteredComponentList<VideoCard> videoCardsFilteredByFreq = new FilteredComponentList();
    private FilteredComponentList<VideoCard> videoCardsFilteredByBusDepth = new FilteredComponentList();
    private FilteredComponentList<VideoCard> videoCardsFilteredByMemory = new FilteredComponentList();

    @PostConstruct
    private void postConstruct() {
        initMenuButtonInterface();
        initMenuButtonProducer();
        initMenuButtonPowerPins();
        initMenuButtonChipset();
        currentComponentList = dao.getAll();
        updateViewList();
    }


    protected void readTextFieldMinFreq() {
        try {
            int minFreq = Util.readIntFormTextField(textFieldMinFreq);
            Util.throwExIfLessOrEq(minFreq, 0);
            videoCardsFilteredByFreq.setComponents(dao.getVideoCardsByFreq(minFreq));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_FREQ_ERROR);
        } catch (EmptyFieldException e) {
            videoCardsFilteredByFreq.resetComponents();
        }
    }

    protected void readTextFieldMinBusDepth() {
        try {
            int minDepth = Util.readIntFormTextField(textFieldMinBusDepth);
            Util.throwExIfLessOrEq(minDepth, 0);
            videoCardsFilteredByBusDepth.setComponents(dao.getVideoCardsByBusDepth(minDepth));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_DEPTH_ERROR);
        } catch (EmptyFieldException e) {
            videoCardsFilteredByBusDepth.resetComponents();
        }
    }

    protected void readTextFieldMinMemory() {
        try {
            int minMemory = Util.readIntFormTextField(textFieldMinMemory);
            Util.throwExIfLessOrEq(minMemory, 0);
            videoCardsFilteredByMemory.setComponents(dao.getVideoCardsByMemorySize(minMemory));
        } catch (NumberFormatException e) {
            Util.showErrorDialog(ERROR, MIN_MEMORY_ERROR);
        } catch (EmptyFieldException e) {
            videoCardsFilteredByMemory.resetComponents();
        }
    }

    protected void initMenuButtonProducer() {
        initMenuButton(menuButtonProducer, videoCardsFilteredByProducer, new InitMenuButtonHelper() {
            @Override
            List<VideoCard> getFilteredComponents(String name) {
                return dao.getByProducer(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableProducers();
            }
        });
    }

    protected void initMenuButtonChipset() {
        initMenuButton(menuButtonChipset, videoCardsFilteredByChipset, new InitMenuButtonHelper() {
            @Override
            List<VideoCard> getFilteredComponents(String name) {
                return dao.getVideoCardsByVideoChipset(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableVideoChipsets();
            }
        });
    }

    protected void initMenuButtonPowerPins() {
        initMenuButton(menuButtonPowerPins, videoCardsFilteredByPowerPins, new InitMenuButtonHelper() {
            @Override
            List<VideoCard> getFilteredComponents(String name) {
                return dao.getVideoCardsByPowerPins(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailablePowerPins();
            }
        });
    }

    protected void initMenuButtonInterface() {
        initMenuButton(menuButtonInterface, videoCardsFilteredByInterface, new InitMenuButtonHelper() {
            @Override
            List<VideoCard> getFilteredComponents(String name) {
                return dao.getVideoCardsByInterfaceConnection(name);
            }

            @Override
            List<String> getMenuItemNames() {
                return dao.getAvailableInterfacesConnection();
            }
        });
    }

    @Override
    protected void readControls() {
        readTextFieldMinBusDepth();
        readTextFieldMinFreq();
        readTextFieldMinMemory();
    }
}
