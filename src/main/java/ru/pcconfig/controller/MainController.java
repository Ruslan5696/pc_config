package ru.pcconfig.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pcconfig.controller.utils.Util;
import ru.pcconfig.controller.utils.WarningVBox;
import ru.pcconfig.model.ApplicationContextProvider;
import ru.pcconfig.model.PcConfiguration;
import ru.pcconfig.model.dto.*;

import javax.annotation.PostConstruct;
import java.io.File;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * Created by Ruslan on 04.03.2017.
 */
public class MainController extends Controller {

    private static final String PROCESSOR_MOTHERBOARD_NOT_COMPARE = "��������� �� ��������� � ����������� ������";
    private static final String PROCESSOR_RAM_NOT_COMPARE = "��������� �� ��������� � ������������ �������";
    private static final String MOTHERBOARD_RAM_NOT_COMPARE = "����������� ����� �� ���������� � ����������� �������";
    private static final String PROCESSOR_SOCKET_IS = "Socket ����������: ";
    private static final String PROCESSOR_SUPPORT_RAM = "��������� ������������ ��������� ���� ������: ";
    private static final String RAM_TYPE_IS = "������������ ������ ����� ���: ";
    private static final String RAM_FORM_FACTOR_IS = "���� ������ ������������ ������: ";
    private static final String MOTHERBOARD_RAM_FORM_FACTOR_IS = "���� ������ ������������ ������ � ����������� �����: ";
    private static final String MOTHERBOARD_DDR3_AMOUNT_IS = "���������� ������ DDR3: ";
    private static final String MOTHERBOARD_DDR4_AMOUNT_IS = "���������� ������ DDR4: ";
    private static final String MOTHERBOARD_SOCKET_IS = "Socket ����������� �����: ";
    private static final String MOTHERBOARD_POWER_SUPPLY_NOT_COMPARE = "����������� ����� � ���� ������� �� ����������";
    private static final String VIDEO_CARD_POWER_SUPPLY_NOT_COMPARE = "���������� � ���� ������� �� ����������";
    private static final String MOTHERBOARD_POWER_PIN_REQ_IS = "������ ������� ����������� �����: ";
    private static final String POWER_SUPPLY_PIN_IS = "���� ������� ������������: ";
    private static final String VIDEO_CARD_PINS_REQ_IS = "������ ������� ����������: ";

    @Autowired
    PcConfiguration pcConfiguration;

    private Stage mainStage;


    private Stage componentStage;
    @Autowired
    private ComponentController componentController;
    @FXML
    HBox hBoxProcessor;
    @FXML
    HBox hBoxMotherboard;
    @FXML
    HBox hBoxHdd;
    @FXML
    HBox hBoxVideoCard;
    @FXML
    HBox hBoxRam;
    @FXML
    HBox hBoxPowerSupplies;
    @FXML
    ImageView imageViewProcessor;
    @FXML
    ImageView imageViewVideoCard;
    @FXML
    ImageView imageViewMotherboard;
    @FXML
    ImageView imageViewRam;
    @FXML
    ImageView imageViewPowerSupplies;
    @FXML
    ImageView imageViewHdd;
    @FXML
    VBox vBoxProcessorDescription;
    @FXML
    VBox vBoxMotherboardDescription;
    @FXML
    VBox vBoxHddDescription;
    @FXML
    VBox vBoxVideoCardDescription;
    @FXML
    VBox vBoxPowerSuppliesDescription;
    @FXML
    VBox vBoxRamDescription;
    @FXML
    @WarningVBox
    VBox vBoxProcessorWarning;
    @FXML
    @WarningVBox
    VBox vBoxMotherboardWarning;
    @FXML
    @WarningVBox
    VBox vBoxVideoCardWarning;
    @FXML
    @WarningVBox
    VBox vBoxHddWarning;
    @FXML
    @WarningVBox
    VBox vBoxRamWarning;
    @FXML
    @WarningVBox
    VBox vBoxPowerSupplyWarning;
    @FXML
    MenuItem menuItemNew;
    @FXML
    MenuItem menuItemSave;
    @FXML
    MenuItem menuItemLoad;
    @FXML
    MenuItem menuItemAbout;
    @FXML
    MenuItem menuItemExit;


    @PostConstruct
    private void postConstruct() {
        imageInit();
        initListeners();
        updateDescriptions();
        initMenuItems();
        componentController.setMainController(this);
    }

    private void updateDescriptions() {
        setComponentShortDescription(pcConfiguration.getProcessor(), vBoxProcessorDescription);
        setComponentShortDescription(pcConfiguration.getMotherboard(), vBoxMotherboardDescription);
        setComponentShortDescription(pcConfiguration.getHdd(), vBoxHddDescription);
        setComponentShortDescription(pcConfiguration.getPowerSupplies(), vBoxPowerSuppliesDescription);
        setComponentShortDescription(pcConfiguration.getRam(), vBoxRamDescription);
        setComponentShortDescription(pcConfiguration.getVideoCard(), vBoxVideoCardDescription);


    }

    private void initMenuItems() {
        menuItemSave.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("VCR files (*.vcr)", "*.vcr");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File file = fileChooser.showSaveDialog(mainStage);


            if (file != null) {
                pcConfiguration.save(pcConfiguration, file);
            }
        });


        menuItemLoad.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();

           // Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("VCR files (*.vcr)", "*.vcr");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File file = fileChooser.showOpenDialog(mainStage);


            if (file != null) {
                pcConfiguration= pcConfiguration.load(file);
                updateDescriptions();
                clearWarnings();
            }
        });

        menuItemNew.setOnAction(event -> {
            pcConfiguration.clear();
            updateDescriptions();
            clearWarnings();
        });

        menuItemAbout.setOnAction(event -> {
            Util.showInfoDialog("� ���������", "�����: ��������� ������\nIcons created by Setyo Ari Wibowo from Noun Project\n������ 1.0");
        });
        menuItemExit.setOnAction(event -> {
            System.exit(0);
        });


    }

    private void initListeners() {
        initHBoxProcessor();
        initHBoxMotherboard();
        initHBoxHdd();
        initHBoxVideoCard();
        initHBoxRam();
        initHBoxPowerSupplies();
    }

    private void initHBoxProcessor() {
        hBoxProcessor.setOnMouseClicked(event -> {

            componentController.setCurrentPcComponent(pcConfiguration.getProcessor());
            componentController.setComponentFilter((((ProcessorFiltersController) ApplicationContextProvider.getApplicationContext().getBean("processorFilterController"))));
            long time = System.currentTimeMillis();
            try {
                Thread.sleep(124);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("����� �������� ����: "+(System.currentTimeMillis()-time)+" ms");
            showChooseComponentDialog();


        });
        initMouseEntered(hBoxProcessor);
    }

    private void initHBoxHdd() {
        hBoxHdd.setOnMouseClicked(event -> {

            componentController.setCurrentPcComponent(pcConfiguration.getHdd());
            componentController.setComponentFilter((((HddFiltersController) ApplicationContextProvider.getApplicationContext().getBean("hddFilterController"))));
            showChooseComponentDialog();
        });
        initMouseEntered(hBoxHdd);
    }

    private void initHBoxMotherboard() {
        hBoxMotherboard.setOnMouseClicked(event -> {

            componentController.setCurrentPcComponent(pcConfiguration.getMotherboard());
            componentController.setComponentFilter((((MotherboardFilterController) ApplicationContextProvider.getApplicationContext().getBean("motherboardFilterController"))));
            showChooseComponentDialog();
        });
        initMouseEntered(hBoxMotherboard);
    }

    private void initHBoxPowerSupplies() {
        hBoxPowerSupplies.setOnMouseClicked(event -> {
            componentController.setCurrentPcComponent(pcConfiguration.getPowerSupplies());
            componentController.setComponentFilter((((PowerSuppliesFilterController) ApplicationContextProvider.getApplicationContext().getBean("powerSuppliesFilterController"))));
            showChooseComponentDialog();
        });
        initMouseEntered(hBoxPowerSupplies);
    }

    private void initHBoxVideoCard() {
        hBoxVideoCard.setOnMouseClicked(event -> {
            componentController.setCurrentPcComponent(pcConfiguration.getVideoCard());
            componentController.setComponentFilter((((VideoCardFiltersController) ApplicationContextProvider.getApplicationContext().getBean("videoCardFilterController"))));
            showChooseComponentDialog();
        });
        initMouseEntered(hBoxVideoCard);
    }

    private void initHBoxRam() {
        hBoxRam.setOnMouseClicked(event -> {
            componentController.setCurrentPcComponent(pcConfiguration.getRam());
            componentController.setComponentFilter((((RamFilterController) ApplicationContextProvider.getApplicationContext().getBean("ramFilterController"))));
            showChooseComponentDialog();
        });
        initMouseEntered(hBoxRam);
    }


    private void initMouseEntered(final HBox hBox) {
        hBox.setOnMouseEntered(event -> hBox.setStyle("-fx-background-color: #DCDCDC;"));
        hBox.setOnMouseExited(event -> hBox.setStyle("-fx-background-color: #F5F5F5;"));
    }

    public Stage getComponentStage() {
        return componentStage;
    }


    private void imageInit() {
        File file = new File(".\\res\\images\\processor.png");
        imageViewProcessor.setImage(new Image(file.toURI().toString()));
        file = new File(".\\res\\images\\mother.png");
        imageViewMotherboard.setImage(new Image(file.toURI().toString()));
        file = new File(".\\res\\images\\ram.png");
        imageViewRam.setImage(new Image(file.toURI().toString()));
        file = new File(".\\res\\images\\videoCard.png");
        imageViewVideoCard.setImage(new Image(file.toURI().toString()));
        file = new File(".\\res\\images\\hdd.png");
        imageViewHdd.setImage(new Image(file.toURI().toString()));
        file = new File(".\\res\\images\\power.png");
        imageViewPowerSupplies.setImage(new Image(file.toURI().toString()));
    }

    private void clearWarnings() {
        Field[] declaredFields = this.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.getAnnotation(WarningVBox.class) != null) {
                field.setAccessible(true);
                try {
                    ((VBox) field.get(this)).getChildren().clear();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void checkComponentsCompatibility() {
        clearWarnings();
        pcConfiguration.checkComponentsCompatibility();
        for (Map.Entry<String, String> entry : pcConfiguration.getMotherboardWarnings().entrySet()) {
            vBoxMotherboardWarning.getChildren().add(Util.createWarningLabel(entry.getKey(), entry.getValue()));
        }
        for (Map.Entry<String, String> entry : pcConfiguration.getHddWarnings().entrySet()) {
            vBoxHddWarning.getChildren().add(Util.createWarningLabel(entry.getKey(), entry.getValue()));
        }
        for (Map.Entry<String, String> entry : pcConfiguration.getPowerSuppliesWarnings().entrySet()) {
            vBoxPowerSupplyWarning.getChildren().add(Util.createWarningLabel(entry.getKey(), entry.getValue()));
        }
        for (Map.Entry<String, String> entry : pcConfiguration.getRamWarnings().entrySet()) {
            vBoxRamWarning.getChildren().add(Util.createWarningLabel(entry.getKey(), entry.getValue()));
        }
        for (Map.Entry<String, String> entry : pcConfiguration.getVideoCardWarnings().entrySet()) {
            vBoxVideoCardWarning.getChildren().add(Util.createWarningLabel(entry.getKey(), entry.getValue()));
        }
        for (Map.Entry<String, String> entry : pcConfiguration.getProcessorWarnings().entrySet()) {
            vBoxProcessorWarning.getChildren().add(Util.createWarningLabel(entry.getKey(), entry.getValue()));
        }
    }


    public void setMainStage(Stage stage) {
        mainStage = stage;
    }

    private void showChooseComponentDialog() {
        if (componentStage == null)
            createComponentStage();
        componentController.showChooseComponentDialog();
    }

    private void createComponentStage() {
        componentStage = new Stage();
        componentController.componentStageInit(componentStage, mainStage);
    }


    public ComponentController getComponentController() {
        return componentController;
    }

    public void setComponentController(ComponentController componentController) {
        this.componentController = componentController;

    }


    public void setPcComponent(AbstractPcComponent pcComponent) {
        VBox vBoxDescription = null;
        if (pcComponent instanceof Processor) {
            vBoxDescription = vBoxProcessorDescription;
            pcConfiguration.setProcessor((Processor) pcComponent);
        } else if (pcComponent instanceof Motherboard) {
            vBoxDescription = vBoxMotherboardDescription;
            pcConfiguration.setMotherboard((Motherboard) pcComponent);
        } else if (pcComponent instanceof Ram) {
            vBoxDescription = vBoxRamDescription;
            pcConfiguration.setRam((Ram) pcComponent);
        } else if (pcComponent instanceof VideoCard) {
            vBoxDescription = vBoxVideoCardDescription;
            pcConfiguration.setVideoCard((VideoCard) pcComponent);
        } else if (pcComponent instanceof Hdd) {
            vBoxDescription = vBoxHddDescription;
            pcConfiguration.setHdd((Hdd) pcComponent);
        } else if (pcComponent instanceof PowerSupplies) {
            vBoxDescription = vBoxPowerSuppliesDescription;
            pcConfiguration.setPowerSupplies((PowerSupplies) pcComponent);
        }
        setComponentShortDescription(pcComponent, vBoxDescription);
        checkComponentsCompatibility();
    }


    private void setComponentShortDescription(AbstractPcComponent component, VBox vBoxDescription) {
        vBoxDescription.getChildren().clear();
        List<Label> labels = Util.createDescriptionLabels(component, component.getParametersForShortDescription());
        vBoxDescription.getChildren().addAll(Util.setShortDescriptionStyle(labels));
    }


}
