package ru.pcconfig.model;

import ru.pcconfig.controller.MainController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Ruslan on 04.03.2017.
 */
public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        MainController mainController = (MainController) ApplicationContextProvider.getApplicationContext().getBean("mainController");
        mainController.setMainStage(primaryStage);


        Scene scene = new Scene(mainController.getRoot(), 600, 400);

        primaryStage.setTitle("Title");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
