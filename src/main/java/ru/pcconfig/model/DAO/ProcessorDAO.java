package ru.pcconfig.model.dao;

import ru.pcconfig.model.dto.Processor;

import java.util.List;

/**
 * Created by Ruslan on 14.03.2017.
 */
public interface ProcessorDAO extends ComponentDAO<Processor> {

    List<Processor> getProcessorsBySocket(String socket);

    List<Processor> getProcessorsByCoreAmount(int coreAmount);

    List<Processor> getProcessorsByMemoryType(String ddrType);

    List<Processor> getProcessorsByFrequency(double notLessThenFrequency);

    List<Processor> getProcessorsByGraphicCore(boolean graphicCore);

    List<Processor> getProcessorsByTdp(int notMoreThenTdp);

    List<String> getAvailableSockets();

    List<String> getAvailableMemoryTypes();

}
