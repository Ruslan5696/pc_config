package ru.pcconfig.model.dao;

import ru.pcconfig.model.dto.PowerSupplies;

import java.util.List;

/**
 * Created by Ruslan on 16.03.2017.
 */
public interface PowerSuppliesDAO extends ComponentDAO<PowerSupplies> {

    List<PowerSupplies> getPowerSuppliesByPower(int notLessThenPower);

    List<PowerSupplies> getPowerSuppliesMotherboardPins(String motherboardPins);

    List<PowerSupplies> getPowerSuppliesVideoCardPins(String videoCardPins);

    List<String> getAvailableMotherboardPins();

    List<String> getAvailableVideoCardPins();
}
