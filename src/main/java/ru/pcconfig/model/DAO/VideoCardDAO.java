package ru.pcconfig.model.dao;

import ru.pcconfig.model.dto.VideoCard;

import java.util.List;

/**
 * Created by Ruslan on 16.03.2017.
 */
public interface VideoCardDAO extends ComponentDAO<VideoCard> {


    List<VideoCard> getVideoCardsByInterfaceConnection(String interfaceConnection);

    List<VideoCard> getVideoCardsByMemorySize(int memorySize);

    List<VideoCard> getVideoCardsByPowerPins(String power);

    List<VideoCard> getVideoCardsByBusDepth(int busDepth);

    List<VideoCard> getVideoCardsByFreq(int freq);

    List<VideoCard> getVideoCardsByRecommendedPower(int recommendedPower);

    List<VideoCard> getVideoCardsByVideoChipset(String videoChipset);

    List<String> getAvailableInterfacesConnection();

    List<String> getAvailablePowerPins();

    List<String> getAvailableVideoChipsets();
}
