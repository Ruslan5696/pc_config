package ru.pcconfig.model.dao.DAOImpl;

/**
 * Created by Ruslan on 16.03.2017.
 */
import ru.pcconfig.model.dao.VideoCardDAO;
import ru.pcconfig.model.dto.VideoCard;

import java.util.List;
public class VideoCardDAOImpl extends AbstractComponentDAO<VideoCard> implements VideoCardDAO{

    public VideoCardDAOImpl(){
        super(VideoCard.class);
    }
    @Override
    public List<VideoCard> getVideoCardsByInterfaceConnection(String interfaceConnection) {
        return getComponentsWhereColumnValueEqToValue( "interfaceConnection", interfaceConnection);
    }

    @Override
    public List<VideoCard> getVideoCardsByMemorySize(int memorySize) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("memorySize", memorySize);
    }

    @Override
    public List<VideoCard> getVideoCardsByPowerPins(String power) {
        return getComponentsWhereColumnValueEqToValue( "power", power);
    }

    @Override
    public List<VideoCard> getVideoCardsByBusDepth(int busDepth) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("busDepth", busDepth);
    }

    @Override
    public List<VideoCard> getVideoCardsByFreq(int freq) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("busDepth", freq);
    }

    @Override
    public List<VideoCard> getVideoCardsByRecommendedPower(int recommendedPower) {
        return getComponentsWhereColumnIntValueLessOrEqToIntValue("recommendedPower", recommendedPower);
    }

    @Override
    public List<VideoCard> getVideoCardsByVideoChipset(String videoChipset) {
        return getComponentsWhereColumnValueEqToValue( "videoChipset", videoChipset);
    }

    @Override
    public List<String> getAvailableInterfacesConnection() {
        return getAvailableValuesFromColumn("interfaceConnection");
    }

    @Override
    public List<String> getAvailablePowerPins() {
        return getAvailableValuesFromColumn("power");
    }

    @Override
    public List<String> getAvailableVideoChipsets() {
        return getAvailableValuesFromColumn("videoChipset");
    }
}
