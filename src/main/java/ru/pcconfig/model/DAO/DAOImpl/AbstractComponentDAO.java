package ru.pcconfig.model.dao.DAOImpl;

import ru.pcconfig.model.dao.ComponentDAO;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.pcconfig.model.dto.AbstractPcComponent;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Ruslan on 16.03.2017.
 */
public class AbstractComponentDAO<T extends AbstractPcComponent> implements ComponentDAO<T> {
    protected SessionFactory sessionFactory;
    protected Class<T> componentClass;

    public AbstractComponentDAO(Class<T> componentClass){
        this.componentClass = componentClass;
    }
    @Override
    public List<T> getByProducer(String producer) {
        return getComponentsWhereColumnValueEqToValue("producer", producer);
    }

    @Override
    public T getByName(String name) {
        return null;
    }

    @Override
    public List<T> getAll() {
        Session session = sessionFactory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(componentClass);
        Root<T> root = criteriaQuery.from(componentClass);
        criteriaQuery.select(root);
        List<T> result =  session.createQuery(criteriaQuery).getResultList();
        session.close();
        return result;
    }

    @Override
    public boolean delete(T t) {

        try (Session session = sessionFactory.openSession();) {
            session.delete(t);
            return true;
        } catch (HibernateException e) {
            e.printStackTrace();
            return false;
        }

    }



    @Override
    public boolean delete(List<T> list) {
        Session session = sessionFactory.openSession();
        return false;
    }

    @Override
    public boolean insert(T t) {

        Transaction transaction = null;
        try (Session session = sessionFactory.openSession();) {
            transaction = session.beginTransaction();
            session.save(t);
            transaction.commit();
            return true;
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean insertList(List<T> list) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            for (T t : list) {
                session.save(t);
            }
            transaction.commit();
            return true;
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
            return false;
        } finally {
            session.close();
        }


    }

    @Override
    public List<String> getAvailableProducers() {
        return getAvailableValuesFromColumn("producer");
    }

    protected List<T> getComponentsWhereColumnValueEqToValue(String columnName, Object value){
        Session session = sessionFactory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(componentClass);
        Root<T> root = criteriaQuery.from(componentClass);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get(columnName), value));
        List<T> result =  session.createQuery(criteriaQuery).getResultList();
        session.close();
        return result;
    }



    protected List<T> getComponentsWhereColumnIntValueGreOrEqToIntValue(String columnName, double value) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(componentClass);
        Root<T> root = criteriaQuery.from(componentClass);
        criteriaQuery.select(root);
        Path<Double> memory = root.get(columnName);
        criteriaQuery.where(criteriaBuilder.ge(memory, value));
        List<T> result =  session.createQuery(criteriaQuery).getResultList();
        session.close();
        return result;
    }

    protected List<T> getComponentsWhereColumnIntValueLessOrEqToIntValue(String columnName, double value) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(componentClass);
        Root<T> root = criteriaQuery.from(componentClass);
        criteriaQuery.select(root);
        Path<Double> memory = root.get(columnName);
        criteriaQuery.where(criteriaBuilder.le(memory, value));
        List<T> result =  session.createQuery(criteriaQuery).getResultList();
        session.close();
        return result;
    }

    protected List<String> getAvailableValuesFromColumn(String columnName){
        Session session = sessionFactory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<String> criteriaQuery = criteriaBuilder.createQuery(String.class);
        Root<T> root = criteriaQuery.from(componentClass);
        criteriaQuery.groupBy(root.get(columnName));
        criteriaQuery.where(criteriaBuilder.isNotNull(root.get(columnName)));
        criteriaQuery.select(root.<String>get(columnName));
        List<String> result =  session.createQuery(criteriaQuery).getResultList();
        session.close();
        return result;

    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}

