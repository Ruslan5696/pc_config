package ru.pcconfig.model.dao.DAOImpl;

/**
 * Created by Ruslan on 16.03.2017.
 */

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.pcconfig.model.dto.Motherboard;
import ru.pcconfig.model.dao.MotherboardDAO;

import java.util.List;
public class MotherboardDAOImpl extends AbstractComponentDAO<Motherboard> implements MotherboardDAO {

    public static void main(String[] args) {
        ApplicationContext contextDb = new ClassPathXmlApplicationContext("db_context.xml");
        MotherboardDAO motherboardDAO = (MotherboardDAO) contextDb.getBean("motherboardDAO");
        System.out.println(motherboardDAO.getMotherboardsByPciE20x16Amount(2));
    }

    public MotherboardDAOImpl(){
        super(Motherboard.class);
    }

    @Override
    public List<Motherboard> getMotherboardsBySocket(String socket) {
        return getComponentsWhereColumnValueEqToValue( "socket", socket);
    }

    @Override
    public List<Motherboard> getMotherboardsByDdr3Amount(int notLessThenDdr3Amount) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("ddr3Amount", notLessThenDdr3Amount);
    }

    @Override
    public List<Motherboard> getMotherboardsByDdr4Amount(int notLessThenDdr4Amount) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("ddr4Amount", notLessThenDdr4Amount);
    }

    @Override
    public List<Motherboard> getMotherboardsByFormFactor(String formFactor) {
        return getComponentsWhereColumnValueEqToValue( "formFactor", formFactor);
    }

    @Override
    public List<Motherboard> getMotherboardsByPciAmount(int notLessThenPciAmount) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("pciAmount", notLessThenPciAmount);
    }

    @Override
    public List<Motherboard> getMotherboardsByPciE20x16Amount(int notLessThenPciE20x16Amount) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("pciE20x16Amount", notLessThenPciE20x16Amount);
    }

    @Override
    public List<Motherboard> getMotherboardsByPciE30x16Amount(int notLessThenPciE30x16Amount) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("pciE30x16Amount", notLessThenPciE30x16Amount);
    }

    @Override
    public List<Motherboard> getMotherboardsBySata2Amount(int notLessThenSata2Amount) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("sata2Amount", notLessThenSata2Amount);
    }

    @Override
    public List<Motherboard> getMotherboardsBySata3Amount(int notLessThenSata3Amount) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("sata3Amount", notLessThenSata3Amount);
    }

    @Override
    public List<Motherboard> getMotherboardsByPowerPins(String power) {
        return getComponentsWhereColumnValueEqToValue( "power", power);
    }

    @Override
    public List<String> getAvailablePowerPins() {
        return getAvailableValuesFromColumn("power");
    }

    @Override
    public List<String> getAvailableFormFactors() {
        return getAvailableValuesFromColumn("formFactor");
    }

    @Override
    public List<String> getAvailableSockets() {
        return getAvailableValuesFromColumn("socket");
    }
}
