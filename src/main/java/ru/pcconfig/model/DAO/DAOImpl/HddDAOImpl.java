package ru.pcconfig.model.dao.DAOImpl;

/**
 * Created by Ruslan on 16.03.2017.
*/
import org.hibernate.Session;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.pcconfig.model.dto.Hdd;
import ru.pcconfig.model.dao.HddDAO;

import java.util.List;
public class HddDAOImpl extends AbstractComponentDAO<Hdd> implements HddDAO {
    Session session;

    public HddDAOImpl(){
        super(Hdd.class);
    }
    @Override
    public List<Hdd> getHddsByFormFactor(String formFactor) {
        return getComponentsWhereColumnValueEqToValue("formFactor", formFactor);
    }

    @Override
    public List<Hdd> getHddsByMemorySize(int memorySize) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("memorySize", memorySize);
    }

    @Override
    public List<Hdd> getHddsByInterfaceConnection(String interfaceConnection) {
        return getComponentsWhereColumnValueEqToValue("interfaceConnection", interfaceConnection);
    }

    @Override
    public List<String> getAvailableFormFactors() {
        return getAvailableValuesFromColumn("formFactor");
    }

    @Override
    public List<String> getAvailableInterfaceConnection() {
        return getAvailableValuesFromColumn("interfaceConnection");
    }
}
