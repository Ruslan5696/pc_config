package ru.pcconfig.model.dao.DAOImpl;

/**
 * Created by Ruslan on 16.03.2017.
 */
import ru.pcconfig.model.dao.ProcessorDAO;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.pcconfig.model.dto.Processor;

import java.util.List;

public class ProcessorDAOImpl extends AbstractComponentDAO<Processor> implements ProcessorDAO {

    public static void main(String[] args) {
        ApplicationContext contextDb = new ClassPathXmlApplicationContext("db_context.xml");
        ProcessorDAO processorDAO = (ProcessorDAO) contextDb.getBean("processorDAO");
        System.out.println(processorDAO.getProcessorsByTdp(50));
    }


    public ProcessorDAOImpl(){
        super(Processor.class);
    }
    @Override
    public List<Processor> getProcessorsBySocket(String socket) {
        return getComponentsWhereColumnValueEqToValue( "socket", socket);
    }

    @Override
    public List<Processor> getProcessorsByCoreAmount(int coreAmount) {
        return getComponentsWhereColumnValueEqToValue( "coreAmount", ""+coreAmount);
    }

    @Override
    public List<Processor> getProcessorsByMemoryType(String ddrType) {
        return getComponentsWhereColumnValueEqToValue( "ddrType", ddrType);
    }

    @Override
    public List<Processor> getProcessorsByFrequency(double notLessThenFrequency) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("frequency", notLessThenFrequency);
    }


    @Override
    public List<Processor> getProcessorsByGraphicCore(boolean graphicCore) {
        return getComponentsWhereColumnValueEqToValue( "isGraphicCore", graphicCore);
    }

    @Override
    public List<Processor> getProcessorsByTdp(int notMoreThenTdp) {
        return getComponentsWhereColumnIntValueLessOrEqToIntValue("tdp", notMoreThenTdp);
    }

    @Override
    public List<String> getAvailableSockets() {
        return getAvailableValuesFromColumn("socket");
    }

    @Override
    public List<String> getAvailableMemoryTypes() {
        return getAvailableValuesFromColumn("ddrType");
    }
}
