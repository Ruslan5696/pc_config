package ru.pcconfig.model.dao.DAOImpl;

/**
 * Created by Ruslan on 16.03.2017.
 */

import ru.pcconfig.model.dao.PowerSuppliesDAO;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.pcconfig.model.dto.PowerSupplies;

import java.util.List;
public class PowerSuppliesDAOImpl extends AbstractComponentDAO<PowerSupplies> implements PowerSuppliesDAO {
    public static void main(String[] args) {
        ApplicationContext contextDb = new ClassPathXmlApplicationContext("db_context.xml");
        PowerSuppliesDAO powerSuppliesDAO = (PowerSuppliesDAO) contextDb.getBean("powerSuppliesDAO");
        System.out.println(powerSuppliesDAO.getAvailableVideoCardPins());
    }

    public PowerSuppliesDAOImpl(){
        super(PowerSupplies.class);
    }

    @Override
    public List<PowerSupplies> getPowerSuppliesByPower(int notLessThenPower) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("power", notLessThenPower);
    }

    @Override
    public List<PowerSupplies> getPowerSuppliesMotherboardPins(String motherboardPins) {
        return getComponentsWhereColumnValueEqToValue("motherboardPins", motherboardPins);
    }

    @Override
    public List<PowerSupplies> getPowerSuppliesVideoCardPins(String videoCardPins) {
        return getComponentsWhereColumnValueEqToValue("videoCardPins", videoCardPins);
    }

    @Override
    public List<String> getAvailableMotherboardPins() {
        return getAvailableValuesFromColumn("motherboardPins");
    }

    @Override
    public List<String> getAvailableVideoCardPins() {
        return getAvailableValuesFromColumn("videoCardPins");
    }
}
