package ru.pcconfig.model.dao;

import ru.pcconfig.model.dto.Motherboard;

import java.util.List;

/**
 * Created by Ruslan on 16.03.2017.
 */
public interface MotherboardDAO extends ComponentDAO<Motherboard> {


    List<Motherboard> getMotherboardsBySocket(String socket);

    List<Motherboard> getMotherboardsByDdr3Amount(int notLessThenDdr3Amount);

    List<Motherboard> getMotherboardsByDdr4Amount(int notLessThenDdr4Amount);

    List<Motherboard> getMotherboardsByFormFactor(String formFactor);

    List<Motherboard> getMotherboardsByPciAmount(int notLessThenPciAmount);

    List<Motherboard> getMotherboardsByPciE20x16Amount(int notLessThenPciE20x16Amount);

    List<Motherboard> getMotherboardsByPciE30x16Amount(int notLessThenPciE30x16Amount);

    List<Motherboard> getMotherboardsBySata2Amount(int notLessThenSata2Amount);

    List<Motherboard> getMotherboardsBySata3Amount(int notLessThenSata3Amount);

    List<Motherboard> getMotherboardsByPowerPins(String power);

    List<String> getAvailablePowerPins();

    List<String> getAvailableFormFactors();

    List<String> getAvailableSockets();
}
