package ru.pcconfig.model.dao;

import java.util.List;

/**
 * Created by Ruslan on 14.03.2017.
 */
public interface ComponentDAO<T> {
    List<T> getByProducer(String producer);
    T getByName(String name);
    List<T> getAll();
    boolean delete(T t);
    boolean delete(List<T> list);
    boolean insert(T t);
    boolean insertList(List<T> list);
    List<String> getAvailableProducers();

}
