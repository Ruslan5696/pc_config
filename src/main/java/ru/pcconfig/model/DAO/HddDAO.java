package ru.pcconfig.model.dao;

import ru.pcconfig.model.dto.Hdd;

import java.util.List;

/**
 * Created by Ruslan on 16.03.2017.
 */
public interface HddDAO extends ComponentDAO<Hdd>{


    List<Hdd> getHddsByFormFactor(String formFactor);
    List<Hdd> getHddsByMemorySize(int memorySize);
    List<Hdd> getHddsByInterfaceConnection(String interfaceConnection);
    List<String> getAvailableFormFactors();
    List<String> getAvailableInterfaceConnection();
}
