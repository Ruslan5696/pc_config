package ru.pcconfig.model.dao;

import ru.pcconfig.model.dto.Ram;

import java.util.List;

/**
 * Created by Ruslan on 16.03.2017.
 */
public interface RamDAO extends ComponentDAO<Ram> {

    List<Ram> getRamByFormFactor(String formFactor);

    List<Ram> getRamByMemoryType(String memoryType);

    List<Ram> getRamByMemorySize(int memorySize);

    List<Ram> getRamByModulesAmount(int modulesAmount);

    List<Ram> getRamByFrequency(int frequency);

    List<String> getAvailableDdrTypes();

    List<String> getAvailableFrequency();
    List<String> getAvailableFormFactors();

}
