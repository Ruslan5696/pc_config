package ru.pcconfig.model;

import javafx.scene.layout.VBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.pcconfig.controller.utils.WarningVBox;
import ru.pcconfig.model.dto.*;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ruslan on 15.05.2017.
 */
@Component
public class PcConfiguration implements Serializable {

    private static final String PROCESSOR_MOTHERBOARD_NOT_COMPARE = "��������� �� ��������� � ����������� ������";
    private static final String PROCESSOR_RAM_NOT_COMPARE = "��������� �� ��������� � ������������ �������";
    private static final String MOTHERBOARD_RAM_NOT_COMPARE = "����������� ����� �� ���������� � ����������� �������";
    private static final String PROCESSOR_SOCKET_IS = "Socket ����������: ";
    private static final String PROCESSOR_SUPPORT_RAM = "��������� ������������ ��������� ���� ������: ";
    private static final String RAM_TYPE_IS = "������������ ������ ����� ���: ";
    private static final String RAM_FORM_FACTOR_IS = "���� ������ ������������ ������: ";
    private static final String MOTHERBOARD_RAM_FORM_FACTOR_IS = "���� ������ ������������ ������ � ����������� �����: ";
    private static final String MOTHERBOARD_DDR3_AMOUNT_IS = "���������� ������ DDR3: ";
    private static final String MOTHERBOARD_DDR4_AMOUNT_IS = "���������� ������ DDR4: ";
    private static final String MOTHERBOARD_SOCKET_IS = "Socket ����������� �����: ";
    private static final String MOTHERBOARD_POWER_SUPPLY_NOT_COMPARE = "����������� ����� � ���� ������� �� ����������";
    private static final String VIDEO_CARD_POWER_SUPPLY_NOT_COMPARE = "���������� � ���� ������� �� ����������";
    private static final String MOTHERBOARD_POWER_PIN_REQ_IS = "������ ������� ����������� �����: ";
    private static final String POWER_SUPPLY_PIN_IS = "���� ������� ������������: ";
    private static final String VIDEO_CARD_PINS_REQ_IS = "������ ������� ����������: ";

    @Autowired
    private Motherboard motherboard;
    @Autowired
    private PowerSupplies powerSupplies;
    @Autowired
    private VideoCard videoCard;
    @Autowired
    private Processor processor;
    @Autowired
    private Ram ram;
    @Autowired
    private Hdd hdd;

    Map<String, String> processorWarnings = new HashMap<>();
    Map<String, String> motherboardWarnings = new HashMap<>();
    Map<String, String> hddWarnings = new HashMap<>();
    Map<String, String> powerSuppliesWarnings = new HashMap<>();
    Map<String, String> videoCardWarnings = new HashMap<>();
    Map<String, String> ramWarnings = new HashMap<>();

    public PcConfiguration(Motherboard motherboard, PowerSupplies powerSupplies, VideoCard videoCard, Processor processor, Ram ram, Hdd hdd) {
        this.motherboard = motherboard;
        this.powerSupplies = powerSupplies;
        this.videoCard = videoCard;
        this.processor = processor;
        this.ram = ram;
        this.hdd = hdd;
    }

    public Map<String, String> getProcessorWarnings() {
        return processorWarnings;
    }

    public Map<String, String> getMotherboardWarnings() {
        return motherboardWarnings;
    }

    public Map<String, String> getHddWarnings() {
        return hddWarnings;
    }

    public Map<String, String> getPowerSuppliesWarnings() {
        return powerSuppliesWarnings;
    }

    public Map<String, String> getVideoCardWarnings() {
        return videoCardWarnings;
    }

    public Map<String, String> getRamWarnings() {
        return ramWarnings;
    }

    public Motherboard getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(Motherboard motherboard) {
        this.motherboard = motherboard;
    }

    public PowerSupplies getPowerSupplies() {
        return powerSupplies;
    }

    public void setPowerSupplies(PowerSupplies powerSupplies) {
        this.powerSupplies = powerSupplies;
    }

    public VideoCard getVideoCard() {
        return videoCard;
    }

    public void setVideoCard(VideoCard videoCard) {
        this.videoCard = videoCard;
    }

    public Processor getProcessor() {
        return processor;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public Ram getRam() {
        return ram;
    }

    public void setRam(Ram ram) {
        this.ram = ram;
    }

    public Hdd getHdd() {
        return hdd;
    }

    public void setHdd(Hdd hdd) {
        this.hdd = hdd;
    }

    private int getPinsSum(String pins) {
        if (pins == null) {
            return 0;
        }
        String[] multipliers = pins.split("x");

        int pinsSum;
        //if (multipliers.length > 1) {
        int[] multipliersSum = new int[multipliers.length];
        for (int i = 0; i < multipliers.length; i++) {
            String[] pinsSplited = multipliers[i].split("\\D");
            for (int j = 0; j < pinsSplited.length; j++) {
                if (!pinsSplited[j].equals(""))
                    multipliersSum[i] += Integer.parseInt(pinsSplited[j]);
            }
        }
        pinsSum = multipliersSum[0];
        for (int i = 1; i < multipliersSum.length; i++) {
            pinsSum *= multipliersSum[i];
        }

        return pinsSum;
    }

    private void clearWarnings() {
        processorWarnings.clear();
        motherboardWarnings.clear();
        hddWarnings.clear();
        videoCardWarnings.clear();
        ramWarnings.clear();
        powerSuppliesWarnings.clear();
    }

    public void checkComponentsCompatibility() {
        clearWarnings();
        if (motherboard.getName() != null) {
            if (processor.getName() != null) {
                if (motherboard.getSocket() == null || processor.getSocket() == null || !motherboard.getSocket().replaceAll(" ", "").contains(processor.getSocket().replaceAll(" ", ""))) {
                    processorWarnings.put(PROCESSOR_MOTHERBOARD_NOT_COMPARE, PROCESSOR_SOCKET_IS + processor.getSocket() + "\n" + MOTHERBOARD_SOCKET_IS + motherboard.getSocket());
                    motherboardWarnings.put(PROCESSOR_MOTHERBOARD_NOT_COMPARE, PROCESSOR_SOCKET_IS + processor.getSocket() + "\n" + MOTHERBOARD_SOCKET_IS + motherboard.getSocket());
                }
            }
            if (ram.getName() != null) {
                if (motherboard.getRamType() == null || ram.getFormFactor() == null || !motherboard.getRamType().contains(ram.getFormFactor())) {
                    motherboardWarnings.put(MOTHERBOARD_RAM_NOT_COMPARE, RAM_FORM_FACTOR_IS + ram.getFormFactor() + "\n" + MOTHERBOARD_RAM_FORM_FACTOR_IS + motherboard.getRamType());
                    ramWarnings.put(MOTHERBOARD_RAM_NOT_COMPARE, RAM_FORM_FACTOR_IS + ram.getFormFactor() + "\n" + MOTHERBOARD_RAM_FORM_FACTOR_IS + motherboard.getRamType());
                } else if (ram.getMemoryType().toUpperCase().contains("DDR3") && motherboard.getDdr3Amount() == 0) {
                    ramWarnings.put(MOTHERBOARD_RAM_NOT_COMPARE, RAM_TYPE_IS + ram.getMemoryType() + "\n" + MOTHERBOARD_DDR3_AMOUNT_IS + motherboard.getDdr3Amount());
                    motherboardWarnings.put(MOTHERBOARD_RAM_NOT_COMPARE, RAM_TYPE_IS + ram.getMemoryType() + "\n" + MOTHERBOARD_DDR3_AMOUNT_IS + motherboard.getDdr3Amount());
                } else if (ram.getMemoryType().toUpperCase().contains("DDR4") && motherboard.getDdr4Amount() == 0) {
                    ramWarnings.put(MOTHERBOARD_RAM_NOT_COMPARE, RAM_TYPE_IS + ram.getMemoryType() + "\n" + MOTHERBOARD_DDR4_AMOUNT_IS + motherboard.getDdr4Amount());
                    motherboardWarnings.put(MOTHERBOARD_RAM_NOT_COMPARE, RAM_TYPE_IS + ram.getMemoryType() + "\n" + MOTHERBOARD_DDR4_AMOUNT_IS + motherboard.getDdr4Amount());
                }
            }
//?????????
            if (hdd.getName() != null) {
                if (!(motherboard.getSata2Amount() > 0 || motherboard.getSata3Amount() > 0)) {

                }
            }
            if (powerSupplies.getName() != null) {
                int pinsSumPowerSupply = getPinsSum(powerSupplies.getMotherboardPins());
                int pinsSumMotherboard = getPinsSum(motherboard.getPower());
                if (pinsSumMotherboard > pinsSumPowerSupply) {
                   motherboardWarnings.put(MOTHERBOARD_POWER_SUPPLY_NOT_COMPARE, MOTHERBOARD_POWER_PIN_REQ_IS + motherboard.getPower() + "\n" + POWER_SUPPLY_PIN_IS + powerSupplies.getMotherboardPins());
                    powerSuppliesWarnings.put(MOTHERBOARD_POWER_SUPPLY_NOT_COMPARE, MOTHERBOARD_POWER_PIN_REQ_IS + motherboard.getPower() + "\n" + POWER_SUPPLY_PIN_IS + powerSupplies.getMotherboardPins());
                }
            }
        }
        if (processor.getName() != null) {
            if (ram.getName() != null) {
                if (!processor.getDdrType().contains(ram.getMemoryType())) {
                    processorWarnings.put(PROCESSOR_RAM_NOT_COMPARE, PROCESSOR_SUPPORT_RAM + processor.getDdrType() + "\n" + RAM_TYPE_IS + ram.getMemoryType());
                }
            }
        }
        if (videoCard.getName() != null) {
            if (powerSupplies.getName() != null) {
                int videoCardPinsRequire = getPinsSum(videoCard.getPower());
                int powerSupplyPinsProvides = getPinsSum(powerSupplies.getVideoCardPins());
                if (videoCardPinsRequire > powerSupplyPinsProvides) {
                    powerSuppliesWarnings.put(VIDEO_CARD_POWER_SUPPLY_NOT_COMPARE, VIDEO_CARD_PINS_REQ_IS + videoCard.getPower() + "\n" + POWER_SUPPLY_PIN_IS + powerSupplies.getVideoCardPins());
                    videoCardWarnings.put(VIDEO_CARD_POWER_SUPPLY_NOT_COMPARE, VIDEO_CARD_PINS_REQ_IS + videoCard.getPower() + "\n" + POWER_SUPPLY_PIN_IS + powerSupplies.getVideoCardPins());
                }
            }
        }
    }

    public static void save(PcConfiguration pcConfiguration, File file) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));) {
            objectOutputStream.writeObject(pcConfiguration);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void clear(){
        clearWarnings();
        motherboard = (Motherboard) ApplicationContextProvider.getApplicationContext().getBean("motherboard");
        processor = (Processor) ApplicationContextProvider.getApplicationContext().getBean("processor");
        hdd = (Hdd) ApplicationContextProvider.getApplicationContext().getBean("hdd");
        powerSupplies = (PowerSupplies) ApplicationContextProvider.getApplicationContext().getBean("powerSupplies");
        videoCard = (VideoCard) ApplicationContextProvider.getApplicationContext().getBean("videoCard");
        ram = (Ram) ApplicationContextProvider.getApplicationContext().getBean("ram");

    }
    public static PcConfiguration load(File file) {

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));) {
            return (PcConfiguration) objectInputStream.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
