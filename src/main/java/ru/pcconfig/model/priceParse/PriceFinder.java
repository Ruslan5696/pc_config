package ru.pcconfig.model.priceParse;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.pcconfig.model.componentCreatorByRef.ConnectionUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruslan on 15.04.2017.
 */
public class PriceFinder {
    //private String urlFormat = "https://market.yandex.ru/search?clid=545&cvredirect=0&text=powercolor%20radeon%20rx%20460%2C%20axrx%20460%202gbd5-dh%2Foc&local-offers-first=0&deliveryincluded=0&onstock=1";
    private static String urlFormat2 = "https://market.yandex.ru/search?clid=545&cvredirect=0&text=%s&local-offers-first=0&deliveryincluded=0&onstock=1";
    private static String urlFormat1 = "http://torg.mail.ru/search/?q=%s&sort=1";


    public static String prepareComponentName(String componentName) {
        return componentName.replaceAll(" ", "+").replaceAll(",", "%2C").replaceAll("//", "2F").replaceAll("[(]", "%28").replaceAll("[)]", "%29");

    }

    public static List<Price> getComponentPrice(String componentName) {
        String preparedComponentName = prepareComponentName(componentName);
        Document document = null;
        Elements elements;
        String url = String.format(urlFormat1, preparedComponentName);
        try {
            document = ConnectionUtil.getDocument(url);
        } catch (IOException e) {
            e.printStackTrace();
        }


        // System.out.println(document);
        List<Price> prices = new ArrayList<>();
        elements = document.select("div[class=preview-card-line__client-name] a");
        for (Element element : elements) {
            Price price = new Price();
            price.setPrice(Integer.parseInt(element.attr("click_value").replaceAll(" ", "").split("\\D+")[0]));
            price.setHref("http://torg.mail.ru" + element.attr("href"));
            price.setShopName(element.text());
            prices.add(price);

        }

        //System.out.println("https://market.yandex.ru"+href);
//

        return prices;
    }
}
