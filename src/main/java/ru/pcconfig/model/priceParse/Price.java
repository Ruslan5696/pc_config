package ru.pcconfig.model.priceParse;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Ruslan on 15.04.2017.
 */
public class Price {


    public static int checkSumOfStream(InputStream inputStream) throws IOException {
        int c = 0;
        int currentByte;
        while ((currentByte=inputStream.read()) > 0) {
            c = Integer.rotateLeft(c, 1) ^ currentByte;
        }
        return c;
    }

    private int price;
    private String shopName;
    private String href;

    public Price(int price, String shopName, String href) {
        this.price = price;
        this.shopName = shopName;
        this.href = href;
    }

    public Price() {
    }

    @Override
    public String toString() {
        return "Price{" +
                "price=" + price +
                ", shopName='" + shopName + '\'' +
                ", href='" + href + '\'' +
                '}';
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
