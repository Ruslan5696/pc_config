package ru.pcconfig.model.componentCreatorByRef;

import ru.pcconfig.model.dto.PowerSupplies;

/**
 * Created by Ruslan on 12.03.2017.
 */
public class PowerSuppliesCreator extends ComponentCreator<PowerSupplies> {


    private void setPower(String value){
        component.setPower(Integer.valueOf(value.split(" ")[0]));
    }

    private void setMotherboardPins(String value){
        component.setMotherboardPins(value);
    }

    private void setVideoCardPins(String value){
        if (!value.equals("�����������"))
            component.setVideoCardPins(value);
    }

    private void setMolexAmount(String value){
        component.setMolexAmount(Integer.valueOf(value.split(" ")[0]));
    }

    private void setSataAmount(String value){
        component.setSataAmount(Integer.valueOf(value.split(" ")[0]));
    }

    private void setFddAmount(String value){
        component.setFddAmount(Integer.valueOf(value.split(" ")[0]));
    }

    @Override
    protected String getProducerFromTitle(String title) {
        return title.split(" ")[2];
    }

    @Override
    protected String getNameFromTitle(String title) {
        return title.substring("���� ������� ".length(), title.indexOf(','));
    }
}