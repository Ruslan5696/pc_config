package ru.pcconfig.model.componentCreatorByRef;

import ru.pcconfig.model.dto.Ram;

/**
 * Created by Ruslan on 12.03.2017.
 */
public class RamCreator extends ComponentCreator<Ram> {
    public static void main(String[] args) {
        String value = "2x16384 ��";
        String[] values = value.split("x");
        if (values.length > 1) {
            System.out.println(Integer.valueOf(values[0]));
            System.out.println(Integer.valueOf(values[1].split(" ")[0]));
        } else
            System.out.println(Integer.valueOf(values[0].split(" ")[0]));
    }


    private void setFormFactor(String value) {
        component.setFormFactor(value);
    }

    private void setMemoryType(String value) {
        component.setMemoryType(value);
    }


    private void setMemorySize(String value) {

        String[] values = value.split("x");
        if (values.length > 1) {
            int modulesAmount = Integer.valueOf(values[0]);
            component.setModulesAmount(modulesAmount);
            component.setMemorySize(modulesAmount * Integer.valueOf(values[1].split(" ")[0]));
        } else {
            component.setModulesAmount(1);
            component.setMemorySize(Integer.valueOf(values[0].split(" ")[0]));
        }
    }

    private void setLatency(String value) {
        component.setLatency(value);
    }

    private void setFrequency(String value) {
        component.setFrequency(Integer.valueOf(value.toLowerCase().split("�")[0]));
    }

    private void setVoltage(String value) {
        component.setVoltage(Double.valueOf(value.toLowerCase().split("�")[0]));
    }

    @Override
    protected String getProducerFromTitle(String title) {
        //������ ������ CORSAIR Vengeance LED CMU64GX4M4A2666C16 DDR4 - 4x 16�� 2666, DIMM, Ret
        return title.split(" ")[2];

    }

    @Override
    protected String getNameFromTitle(String title) {
        return title.substring(title.indexOf("�") + 2, title.indexOf(","));
    }
}
