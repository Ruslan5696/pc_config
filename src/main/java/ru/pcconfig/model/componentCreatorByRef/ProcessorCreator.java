package ru.pcconfig.model.componentCreatorByRef;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.pcconfig.model.dto.Processor;

import java.io.IOException;

/**
 * Created by Ruslan on 04.03.2017.
 */
public class ProcessorCreator extends ComponentCreator<Processor> {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("pc_param_for_parsing_context.xml");
        ProcessorCreator pcComponentCreator = (ProcessorCreator) context.getBean("voProcCreator");
        try {
            System.out.println(pcComponentCreator.create());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void setCore(String value) {
        component.setCore(value);
    }

    private void setSocket(String value) {
        component.setSocket(value);
    }

    private void setCoreAmount(String value) {
        component.setCoreAmount(Integer.valueOf(value));
    }

    private void setTreadAmount(String value) {
        component.setTreadAmount(Integer.valueOf(value));
    }

    private void setFrequency(String value) {
        //Возможны 2 варианта: с режимом турбо и без
        String values[] = value.split(" ");
        component.setFrequency(Double.valueOf(values[0]));
        if (values.length > 2) {
            component.setFrequencyTurbo(Double.valueOf(values[3]));
        }
    }

    private void setL1cache(String value) {
        component.setL1cache(value);
    }

    private void setL2cache(String value) {
        component.setL2cache(value);
    }

    private void setL3cache(String value) {
        component.setL3cache(value);
    }

    private void setTechProcess(String value) {
        component.setTechProcess(Integer.valueOf(value.split(" ")[0]));
    }

    private void setTdp(String value) {
        component.setTdp(Integer.valueOf(value.split(" ")[0]));
    }

    private void setTypeOfDelivery(String value) {
        component.setTypeOfDelivery(value);
    }

    private void setGraphicCore(String value) {
        component.setGraphicCore(value.toLowerCase().equals("есть"));
    }

    private void setGraphicCoreModel(String value) {
        component.setGraphicCoreModel(value);
    }

    private void setFrequencyGraphicCore(String value) {
        //Возможны 2 варианта: с режимом турбо и без
        String values[] = value.split(" ");
        component.setFrequencyGraphicCore(Integer.valueOf(values[0]));
        if (values.length > 2) {
            component.setFrequencyGraphicCoreTurbo(Integer.valueOf(values[3]));
        }
    }

    private void setDdrType(String value) {
        component.setDdrType(value);
    }


    @Override
    protected String getProducerFromTitle(String title) {
        return title.split(" ")[1];
    }

    @Override
    protected String getNameFromTitle(String title) {
        return title.substring(title.indexOf(" ")+1, title.indexOf(","));
    }
}
