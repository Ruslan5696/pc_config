package ru.pcconfig.model.componentCreatorByRef;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.logging.*;


/**
 * Created by Ruslan on 17.03.2017.
 */
public class ConnectionUtil {

    public static Document getDocument(String url) throws IOException {

        Document document = null;

        for (int i = 0; i < 3; i++) {

            document = Jsoup.connect(url).userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36 OPR/43.0.2442.1144").get();
            return document;

        }
        return null;
    }
}
