package ru.pcconfig.model.componentCreatorByRef;

import ru.pcconfig.model.dto.Motherboard;

/**
 * Created by Ruslan on 11.03.2017.
 */
public class MotherboardCreator extends ComponentCreator<Motherboard> {

    public void setSocket(String value) {
        component.setSocket(value);
    }

    public void setChipset(String value) {
        component.setChipset(value);
    }

    public void setDoubleVideoCard(String value) {
        component.setDoubleVideoCard(value);
    }

    public void setDdr3Amount(String value) {
        component.setDdr3Amount(Integer.valueOf(value));
    }

    public void setDdr4Amount(String value) {
        component.setDdr4Amount(Integer.valueOf(value));
    }

    public void setPciAmount(String value) {
        component.setPciAmount(Integer.valueOf(value));
    }

    public void setPciEX1Amount(String value) {
        component.setPciEX1Amount(Integer.valueOf(value));
    }

    public void setPciE20x16Amount(String value) {
        component.setPciE20x16Amount(Integer.valueOf(value));
    }

    public void setPciE30x16Amount(String value) {
        component.setPciE30x16Amount(Integer.valueOf(value));
    }

    public void setSata2Amount(String value) {
        component.setSata2Amount(Integer.valueOf(value));
    }

    public void setSata3Amount(String value) {
        component.setSata3Amount(Integer.valueOf(value));
    }

    public void setPs2Amount(String value) {
        component.setPs2Amount(Integer.valueOf(value.split(" ")[0]));
    }

    public void setRamType(String value){
        component.setRamType(value);
    }

    public void setUsb2Amount(String value) {
        component.setUsb2Amount(Integer.valueOf(value));
    }

    public void setUsb3Amount(String value) {
        component.setUsb3Amount(Integer.valueOf(value));
    }

    public void setNetworkInterface(String value) {
        component.setNetworkInterface(value);
    }

    public void setAudioController(String value) {
        component.setAudioController(value);
    }

    public void setFormFactor(String value) {
        component.setFormFactor(value);
    }

    public void setPower(String value) {
        component.setPower(value);
    }

    @Override
    protected String getProducerFromTitle(String title) {
        try {
            return title.split(" ")[2];
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected String getNameFromTitle(String title) {
        return title.substring(title.indexOf("�����")+6, title.indexOf(","));
    }
}
