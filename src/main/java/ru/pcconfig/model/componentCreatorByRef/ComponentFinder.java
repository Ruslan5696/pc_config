package ru.pcconfig.model.componentCreatorByRef;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Ruslan on 04.03.2017.
 */
public class ComponentFinder {
    private String urlFormat;

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("pc_param_for_parsing_context.xml");
        ComponentFinder ramComponentFinder = (ComponentFinder) context.getBean("ramFinder");
        ComponentFinder motherboardComponentFinder = (ComponentFinder) context.getBean("motherboardFinder");
        ComponentFinder processorFinder = (ComponentFinder) context.getBean("processorFinder");
        ComponentFinder videoCardFinder = (ComponentFinder) context.getBean("videoCardFinder");
        ComponentFinder hddFinder = (ComponentFinder) context.getBean("hddFinder");
        ComponentFinder powerSuppliesFinder = (ComponentFinder) context.getBean("powerSuppliesFinder");

        //List<String> powerSuppliesRefs = powerSuppliesFinder.findAllRefs();
        //List<String> hddRefs = hddFinder.findAllRefs();
        //List<String> videoCardRefs = videoCardFinder.findAllRefs();
        //List<String> ramRefs = ramComponentFinder.findAllRefs();

        //List<String> processorRefs = processorFinder.findAllRefs();

//        for (int i = 10; i < 20; i++) {
//            PowerSuppliesCreator powerSuppliesCreator = (PowerSuppliesCreator) context.getBean("powerSuppliesCreator");
//            powerSuppliesCreator.setURL(powerSuppliesRefs.get(i));
//            System.out.println(powerSuppliesCreator.create());
//        }

//        for (int i = 10; i < 20; i++) {
//            HddCreator hddCreator = (HddCreator) context.getBean("hddCreator");
//            hddCreator.setURL(hddRefs.get(i));
//            System.out.println(hddCreator.create());
//        }

//        for (int i = 10; i < 20; i++) {
//            VideoCardCreator videoCardCreator = (VideoCardCreator) context.getBean("videoCardCreator");
//            videoCardCreator.setURL(videoCardRefs.get(i));
//            System.out.println(videoCardCreator.create());
//        }

//        for (int i = 0; i < 10; i++) {
//            RamCreator ramCreator = (RamCreator) context.getBean("ramCreator");
//            ramCreator.setURL(ramRefs.get(i));
//            System.out.println(ramCreator.create());
//        }
//
//
//        for (int i = 40; i < 50; i++) {
//            MotherboardCreator motherBoardCreator = (MotherboardCreator) context.getBean("motherboardCreator");
//            motherBoardCreator.setURL(motherboardRefs.get(i));
//            System.out.println(motherBoardCreator.create());
//
//        }


//        for (int i = 0; i < 10; i++) {
//            ProcessorCreator pcComponentCreator = (ProcessorCreator) context.getBean("processorCreator");
//            pcComponentCreator.setURL(processorRefs.get(i));
//            System.out.println(pcComponentCreator.create());
//
//        }
    }


    public List<String> findAllRefsOnPage(String url) throws IOException {
        return findNewRefsOnPage(url, "");
    }

    public int findPageAmount() throws IOException {
        Document document = null;
        int i = 1;
        document = ConnectionUtil.getDocument(String.format(urlFormat, i));
        return Integer.valueOf(document.select("div[class=page_listing] li").last().text());
    }

    public List<String> findNewRefsOnPage(String url, String existing) throws IOException {
    //public List<String> findNewRefsOnPage(String existing, int pageNumber) throws IOException {
        List<String> refs = new ArrayList<>();
        Document document = null;
        Elements elements;
        document = ConnectionUtil.getDocument(url);

        elements = document.select("div[class=product_name cms_item_panel subcategory-product-item__info] a[href]");
        if (elements.size() > 0) {
            for (Element element : elements) {
                String[] name = element.attr("title").split(",* *[�-��-�] *");
                //System.out.println(Arrays.toString(name));
                name[name.length - 1].replaceAll("&nbsp;", " ");
                if (!existing.replaceAll("  ", " ").contains(name[name.length - 1].replaceAll("&nbsp;", " ").replaceAll("&#151;", "-").trim())) {
                    System.out.println(name[name.length - 1].replaceAll("&nbsp;", " ").replaceAll("&#151;", "-"));
                    refs.add(element.attr("href"));
                }
            }

        }
        return refs;
    }


    public void setUrlFormat(String urlFormat) {
        this.urlFormat = urlFormat;
    }
    public String getUrlByPageNumber(int pageNumber){
        return String.format(urlFormat, pageNumber);
    }
    public String getUrlFormat() {
        return urlFormat;
    }

}
