package ru.pcconfig.model.componentCreatorByRef;

import org.springframework.stereotype.Component;
import ru.pcconfig.model.dto.Hdd;

/**
 * Created by Ruslan on 12.03.2017.
 */
@Component
public class HddCreator extends ComponentCreator<Hdd> {


    private void setFormFactor(String value){
        component.setFormFactor(value);
    }

    private void setMemorySize(String value){
        String[] values = value.split(" ");
        if (values[1].equals("��")){
            component.setMemorySize(1000*Integer.parseInt(values[0]));
        }
        component.setMemorySize(Integer.parseInt(values[0]));
    }

    private void setInterfaceConnection(String value){
        component.setInterfaceConnection(value);
    }

    private void setBufferSize(String value){
        component.setBufferSize(Integer.valueOf(value.split("\\D+")[0]));
    }

    private void setRotationSpeed(String value){
        component.setRotationSpeed(Integer.valueOf(value.split("\\D+")[0]));
    }

    @Override
    protected String getProducerFromTitle(String title) {
        return title.split(" ")[2];
    }

    @Override
    protected String getNameFromTitle(String title) {
        return title.substring("������� ���� ".length(), title.indexOf(','));
    }
}
