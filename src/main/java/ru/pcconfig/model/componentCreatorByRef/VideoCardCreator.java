package ru.pcconfig.model.componentCreatorByRef;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.pcconfig.model.dto.VideoCard;

import java.io.IOException;

/**
 * Created by Ruslan on 12.03.2017.
 */
public class VideoCardCreator extends ComponentCreator<VideoCard>{
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("pc_param_for_parsing_context.xml");
        VideoCardCreator pcComponentCreator = (VideoCardCreator) context.getBean("videoCardCreator");
        try {
            System.out.println(pcComponentCreator.create());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void setInterfaceConnection(String value){
        component.setInterfaceConnection(value);
    }

    private void setVideoChipset(String value){
        component.setVideoChipset(value);
    }

    private void setTechProcess(String value){
        component.setTechProcess(Integer.valueOf(value.split("\\D+")[0]));
    }

    private void setFrequency(String value){
        String[] values = value.split("\\D+");
        component.setFrequency(Integer.valueOf(values[0]));
        if (values.length==2){
            component.setFrequencyBoost(Integer.valueOf(values[1]));
        }
    }

    private void setMemorySize(String value){
        String[] values = value.split(" ");
        if (values[1].equals("��")){
            component.setMemorySize(Integer.valueOf(values[0])*1024);
        }else
            component.setMemorySize(Integer.valueOf(values[0]));
    }

    private void setMemoryType(String value){
        component.setMemoryType(value);
    }

    private void setPower(String value){
        if (!value.equals("��� ��������������� �������"))
            component.setPower(value);
    }

    private void setBusDepth(String value){
        component.setBusDepth(Integer.valueOf(value.split("\\D+")[0]));
    }

    private void setDoubleVideoCard(String value){
        component.setDoubleVideoCard(value);
    }

    private void setRecommendedPower(String value){
        component.setRecommendedPower(Integer.valueOf(value.split(" ")[0]));
    }


    @Override
    protected String getProducerFromTitle(String title) {
        //���������� ASUS GeForce GTX 1050TI, STRIX-GTX1050TI-O4G-GAMING, 4��, GDDR5, OC, Ret
        return title.split(" ")[1];
    }

    @Override
    protected String getNameFromTitle(String title) {
        return title.substring(title.indexOf(" ")+1, title.indexOf(',',title.indexOf(',')+1));
    }


}
