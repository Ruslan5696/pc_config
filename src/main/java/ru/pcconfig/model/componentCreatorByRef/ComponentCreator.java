package ru.pcconfig.model.componentCreatorByRef;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.pcconfig.model.dto.AbstractPcComponent;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by Ruslan on 11.03.2017.
 */
public abstract class ComponentCreator<T extends AbstractPcComponent> {
    protected Map<String, String> parameters;
    protected String URL = "https://www.citilink.ru/catalog/computers_and_notebooks/parts/videocards/352622/";
    protected T component;
    protected String componentTitle;

    public void setComponent(T component) {
        this.component = component;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public T create() throws IOException {
        parameters = component.getParametersForParsing();
        Document document = null;
        Elements elements = null;
        document = ConnectionUtil.getDocument(URL);
        //у каждого компонента свой тайтл, поэтому извлечением из него имени и производителя занимается конкретная реализация ComponentCreator
        componentTitle = document.select("meta[property=og:title]").attr("content");
        component.setName(getNameFromTitle(componentTitle));
        component.setProducer(getProducerFromTitle(componentTitle));
        elements = document.select("table[class=product_features] tr");
        if (elements.size() > 0) {
            for (Element element : elements) {
                if (parameters.containsKey(element.select("th > span").text())) {
                    invokeSetterByName(parameters.get(element.select("th > span").text()), element.select("td").text());
                }
            }
        }
        return component;
    }

    protected T createEmptyComponent() {
        return null;
    }

    protected void invokeSetterByName(String methodName, String valueForSetting) {
        Method method = null;
        try {
            method = this.getClass().getDeclaredMethod(methodName, String.class);
            method.setAccessible(true);
            method.invoke(this, valueForSetting);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    abstract protected String getProducerFromTitle(String title);

    abstract protected String getNameFromTitle(String title);


}