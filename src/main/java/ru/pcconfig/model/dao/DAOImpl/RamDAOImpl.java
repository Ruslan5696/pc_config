package ru.pcconfig.model.dao.DAOImpl;

/**
 * Created by Ruslan on 16.03.2017.
 */
import ru.pcconfig.model.dao.RamDAO;
import ru.pcconfig.model.dto.Ram;

import java.util.List;
public class RamDAOImpl extends AbstractComponentDAO<Ram> implements RamDAO {

    public RamDAOImpl(){
        super(Ram.class);
    }

    @Override
    public List<Ram> getRamByFormFactor(String formFactor) {
        return getComponentsWhereColumnValueEqToValue( "formFactor", formFactor);
    }

    @Override
    public List<Ram> getRamByMemoryType(String memoryType) {
        return getComponentsWhereColumnValueEqToValue( "memoryType", memoryType);
    }

    @Override
    public List<Ram> getRamByMemorySize(int memorySize) {
        return getComponentsWhereColumnIntValueGreOrEqToIntValue("memorySize", memorySize);
    }

    @Override
    public List<Ram> getRamByModulesAmount(int modulesAmount) {
        return getComponentsWhereColumnValueEqToValue( "modulesAmount", ""+modulesAmount);
    }

    @Override
    public List<Ram> getRamByFrequency(int frequency) {
        return getComponentsWhereColumnValueEqToValue( "modulesAmount", ""+frequency);
    }


    @Override
    public List<String> getAvailableDdrTypes() {
        return getAvailableValuesFromColumn("memoryType");
    }

    @Override
    public List<String> getAvailableFrequency() {
        return getAvailableValuesFromColumn("frequency");
    }

    @Override
    public List<String> getAvailableFormFactors() {
        return getAvailableValuesFromColumn("formFactor");
    }
}
