package ru.pcconfig.model.dto;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by Ruslan on 04.03.2017.
 */
@Component
@Entity
@Table(name = "processor")
public class Processor extends AbstractPcComponent {
    public static void main(String[] args) {
        ApplicationContext contextDb = new ClassPathXmlApplicationContext("pc_params_for_view_context.xml");
        Map<String, String> map = (Map) contextDb.getBean("processorParamsForView");
        Processor processor = new Processor();
        processor.setCore("Vis");
        processor.setFrequency(3.4);
        processor.setFrequencyTurbo(3.8);
        processor.setCoreAmount(4);

    }




    @Column
    private String core;
    @Column
    private String socket;
    @Column
    private int coreAmount;
    @Column
    private int treadAmount;
    @Column
    private String ddrType;
    @Column
    private double frequency;
    @Column
    private double frequencyTurbo;
    @Column
    private String l1cache;
    @Column
    private String l2cache;
    @Column
    private String l3cache;
    @Column
    private int techProcess;
    @Column
    private int tdp;
    @Column
    private String typeOfDelivery;
    @Column
    private boolean isGraphicCore;
    @Column
    private String graphicCoreModel;
    @Column
    private int frequencyGraphicCore;
    @Column
    private int frequencyGraphicCoreTurbo;

    public String toString() {
        return name+" "+typeOfDelivery;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Processor processor = (Processor) o;

        if (coreAmount != processor.coreAmount) return false;
        if (treadAmount != processor.treadAmount) return false;
        if (Double.compare(processor.frequency, frequency) != 0) return false;
        if (Double.compare(processor.frequencyTurbo, frequencyTurbo) != 0) return false;
        if (techProcess != processor.techProcess) return false;
        if (tdp != processor.tdp) return false;
        if (isGraphicCore != processor.isGraphicCore) return false;
        if (frequencyGraphicCore != processor.frequencyGraphicCore) return false;
        if (frequencyGraphicCoreTurbo != processor.frequencyGraphicCoreTurbo) return false;
        if (core != null ? !core.equals(processor.core) : processor.core != null) return false;
        if (socket != null ? !socket.equals(processor.socket) : processor.socket != null) return false;
        if (ddrType != null ? !ddrType.equals(processor.ddrType) : processor.ddrType != null) return false;
        if (l1cache != null ? !l1cache.equals(processor.l1cache) : processor.l1cache != null) return false;
        if (l2cache != null ? !l2cache.equals(processor.l2cache) : processor.l2cache != null) return false;
        if (l3cache != null ? !l3cache.equals(processor.l3cache) : processor.l3cache != null) return false;
        if (typeOfDelivery != null ? !typeOfDelivery.equals(processor.typeOfDelivery) : processor.typeOfDelivery != null)
            return false;
        return graphicCoreModel != null ? graphicCoreModel.equals(processor.graphicCoreModel) : processor.graphicCoreModel == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = core != null ? core.hashCode() : 0;
        result = 31 * result + (socket != null ? socket.hashCode() : 0);
        result = 31 * result + coreAmount;
        result = 31 * result + treadAmount;
        result = 31 * result + (ddrType != null ? ddrType.hashCode() : 0);
        temp = Double.doubleToLongBits(frequency);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(frequencyTurbo);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (l1cache != null ? l1cache.hashCode() : 0);
        result = 31 * result + (l2cache != null ? l2cache.hashCode() : 0);
        result = 31 * result + (l3cache != null ? l3cache.hashCode() : 0);
        result = 31 * result + techProcess;
        result = 31 * result + tdp;
        result = 31 * result + (typeOfDelivery != null ? typeOfDelivery.hashCode() : 0);
        result = 31 * result + (isGraphicCore ? 1 : 0);
        result = 31 * result + (graphicCoreModel != null ? graphicCoreModel.hashCode() : 0);
        result = 31 * result + frequencyGraphicCore;
        result = 31 * result + frequencyGraphicCoreTurbo;
        return result;
    }

    public void setCore(String core) {
        this.core = core;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public void setCoreAmount(int coreAmount) {
        this.coreAmount = coreAmount;
    }

    public void setTreadAmount(int treadAmount) {
        this.treadAmount = treadAmount;
    }

    public void setDdrType(String ddrType) {
        this.ddrType = ddrType;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    public void setFrequencyTurbo(double frequencyTurbo) {
        this.frequencyTurbo = frequencyTurbo;
    }

    public void setL1cache(String l1cache) {
        this.l1cache = l1cache;
    }

    public void setL2cache(String l2cache) {
        this.l2cache = l2cache;
    }

    public void setL3cache(String l3cache) {
        this.l3cache = l3cache;
    }

    public void setTechProcess(int techProcess) {
        this.techProcess = techProcess;
    }

    public void setTdp(int tdp) {
        this.tdp = tdp;
    }

    public void setTypeOfDelivery(String typeOfDelivery) {
        this.typeOfDelivery = typeOfDelivery;
    }

    public void setGraphicCore(boolean graphicCore) {
        isGraphicCore = graphicCore;
    }

    public void setGraphicCoreModel(String graphicCoreModel) {
        this.graphicCoreModel = graphicCoreModel;
    }

    public void setFrequencyGraphicCore(int frequencyGraphicCore) {
        this.frequencyGraphicCore = frequencyGraphicCore;
    }


    public String getCore() {
        return core;
    }

    public String getSocket() {
        return socket;
    }

    public int getCoreAmount() {
        return coreAmount;
    }

    public int getTreadAmount() {
        return treadAmount;
    }

    public String getDdrType() {
        return ddrType;
    }

    public double getFrequency() {
        return frequency;
    }

    public double getFrequencyTurbo() {
        return frequencyTurbo;
    }

    public String getL1cache() {
        return l1cache;
    }

    public String getL2cache() {
        return l2cache;
    }

    public String getL3cache() {
        return l3cache;
    }

    public int getTechProcess() {
        return techProcess;
    }

    public int getTdp() {
        return tdp;
    }

    public String getTypeOfDelivery() {
        return typeOfDelivery;
    }

    public boolean isGraphicCore() {
        return isGraphicCore;
    }

    public String getGraphicCoreModel() {
        return graphicCoreModel;
    }

    public int getFrequencyGraphicCore() {
        return frequencyGraphicCore;
    }

    public int getFrequencyGraphicCoreTurbo() {
        return frequencyGraphicCoreTurbo;
    }

    public void setFrequencyGraphicCoreTurbo(int frequencyGraphicCoreTurbo) {
        this.frequencyGraphicCoreTurbo = frequencyGraphicCoreTurbo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
