package ru.pcconfig.model.dto;


import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by Ruslan on 12.03.2017.
 */
@Component
@Entity
@Table(name = "power_supplies")
public class PowerSupplies extends AbstractPcComponent {
    @Column
    private int power;
    @Column
    private String motherboardPins;
    @Column
    private String videoCardPins;
    @Column
    private int molexAmount;
    @Column
    private int sataAmount;
    @Column
    private int fddAmount;





    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PowerSupplies that = (PowerSupplies) o;

        if (power != that.power) return false;
        if (molexAmount != that.molexAmount) return false;
        if (sataAmount != that.sataAmount) return false;
        if (fddAmount != that.fddAmount) return false;
        if (motherboardPins != null ? !motherboardPins.equals(that.motherboardPins) : that.motherboardPins != null)
            return false;
        return videoCardPins != null ? videoCardPins.equals(that.videoCardPins) : that.videoCardPins == null;
    }

    @Override
    public int hashCode() {
        int result = power;
        result = 31 * result + (motherboardPins != null ? motherboardPins.hashCode() : 0);
        result = 31 * result + (videoCardPins != null ? videoCardPins.hashCode() : 0);
        result = 31 * result + molexAmount;
        result = 31 * result + sataAmount;
        result = 31 * result + fddAmount;
        return result;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getMotherboardPins() {
        return motherboardPins;
    }

    public void setMotherboardPins(String motherboardPins) {
        this.motherboardPins = motherboardPins;
    }

    public String getVideoCardPins() {
        return videoCardPins;
    }

    public void setVideoCardPins(String videoCardPins) {
        this.videoCardPins = videoCardPins;
    }

    public int getMolexAmount() {
        return molexAmount;
    }

    public void setMolexAmount(int molexAmount) {
        this.molexAmount = molexAmount;
    }

    public int getSataAmount() {
        return sataAmount;
    }

    public void setSataAmount(int sataAmount) {
        this.sataAmount = sataAmount;
    }

    public int getFddAmount() {
        return fddAmount;
    }

    public void setFddAmount(int fddAmount) {
        this.fddAmount = fddAmount;
    }
}
