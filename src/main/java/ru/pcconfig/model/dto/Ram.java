package ru.pcconfig.model.dto;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by Ruslan on 12.03.2017.
 */
@Component
@Entity
@Table(name = "ram")
public class Ram extends AbstractPcComponent {
    @Column
    private String formFactor;
    @Column
    private String memoryType;
    @Column
    private int memorySize; //Мб
    @Column
    private int modulesAmount;
    @Column
    private String latency;
    @Column
    private int frequency; //Мгц
    @Column
    private double voltage; //вольт




    @Override
    public String toString() {
        return name;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ram ram = (Ram) o;

        if (memorySize != ram.memorySize) return false;
        if (modulesAmount != ram.modulesAmount) return false;
        if (frequency != ram.frequency) return false;
        if (Double.compare(ram.voltage, voltage) != 0) return false;
        if (formFactor != null ? !formFactor.equals(ram.formFactor) : ram.formFactor != null) return false;
        if (memoryType != null ? !memoryType.equals(ram.memoryType) : ram.memoryType != null) return false;
        return latency != null ? latency.equals(ram.latency) : ram.latency == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = formFactor != null ? formFactor.hashCode() : 0;
        result = 31 * result + (memoryType != null ? memoryType.hashCode() : 0);
        result = 31 * result + memorySize;
        result = 31 * result + modulesAmount;
        result = 31 * result + (latency != null ? latency.hashCode() : 0);
        result = 31 * result + frequency;
        temp = Double.doubleToLongBits(voltage);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public void setFormFactor(String formFactor) {
        this.formFactor = formFactor;
    }

    public void setMemoryType(String memoryType) {
        this.memoryType = memoryType;
    }

    public void setMemorySize(int memorySize) {
        this.memorySize = memorySize;
    }

    public void setModulesAmount(int modulesAmount) {
        this.modulesAmount = modulesAmount;
    }

    public void setLatency(String latency) {
        this.latency = latency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void setVoltage(double voltage) {
        this.voltage = voltage;
    }

    public String getFormFactor() {
        return formFactor;
    }

    public String getMemoryType() {
        return memoryType;
    }

    public int getMemorySize() {
        return memorySize;
    }

    public int getModulesAmount() {
        return modulesAmount;
    }

    public String getLatency() {
        return latency;
    }

    public int getFrequency() {
        return frequency;
    }

    public double getVoltage() {
        return voltage;
    }
}
