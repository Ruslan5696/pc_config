package ru.pcconfig.model.dto;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by Ruslan on 11.03.2017.
 */
@Component
@Entity
@Table(name = "motherboard")
public class Motherboard extends AbstractPcComponent{
    @Column
    private String socket;
    @Column
    private String chipset;
    @Column
    private String doubleVideoCard;
    @Column
    private String ramType;
    @Column
    private int ddr3Amount;
    @Column
    private int ddr4Amount;
    @Column
    private int pciAmount;
    @Column
    private int pciEX1Amount;
    @Column
    private int pciE20x16Amount;
    @Column
    private int pciE30x16Amount;
    @Column
    private int sata2Amount;
    @Column
    private int sata3Amount;
    @Column
    private int ps2Amount;
    @Column
    private int usb2Amount;
    @Column
    private int usb3Amount;
    @Column
    private String networkInterface;
    @Column
    private String AudioController;
    @Column
    private String formFactor;
    @Column
    private String power;

    public String toString() {
        return name;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Motherboard that = (Motherboard) o;

        if (ddr3Amount != that.ddr3Amount) return false;
        if (ddr4Amount != that.ddr4Amount) return false;
        if (pciAmount != that.pciAmount) return false;
        if (pciEX1Amount != that.pciEX1Amount) return false;
        if (pciE20x16Amount != that.pciE20x16Amount) return false;
        if (pciE30x16Amount != that.pciE30x16Amount) return false;
        if (sata2Amount != that.sata2Amount) return false;
        if (sata3Amount != that.sata3Amount) return false;
        if (ps2Amount != that.ps2Amount) return false;
        if (usb2Amount != that.usb2Amount) return false;
        if (usb3Amount != that.usb3Amount) return false;
        if (socket != null ? !socket.equals(that.socket) : that.socket != null) return false;
        if (chipset != null ? !chipset.equals(that.chipset) : that.chipset != null) return false;
        if (doubleVideoCard != null ? !doubleVideoCard.equals(that.doubleVideoCard) : that.doubleVideoCard != null)
            return false;
        if (ramType != null ? !ramType.equals(that.ramType) : that.ramType != null) return false;
        if (networkInterface != null ? !networkInterface.equals(that.networkInterface) : that.networkInterface != null)
            return false;
        if (AudioController != null ? !AudioController.equals(that.AudioController) : that.AudioController != null)
            return false;
        if (formFactor != null ? !formFactor.equals(that.formFactor) : that.formFactor != null) return false;
        return power != null ? power.equals(that.power) : that.power == null;
    }

    @Override
    public int hashCode() {
        int result = socket != null ? socket.hashCode() : 0;
        result = 31 * result + (chipset != null ? chipset.hashCode() : 0);
        result = 31 * result + (doubleVideoCard != null ? doubleVideoCard.hashCode() : 0);
        result = 31 * result + (ramType != null ? ramType.hashCode() : 0);
        result = 31 * result + ddr3Amount;
        result = 31 * result + ddr4Amount;
        result = 31 * result + pciAmount;
        result = 31 * result + pciEX1Amount;
        result = 31 * result + pciE20x16Amount;
        result = 31 * result + pciE30x16Amount;
        result = 31 * result + sata2Amount;
        result = 31 * result + sata3Amount;
        result = 31 * result + ps2Amount;
        result = 31 * result + usb2Amount;
        result = 31 * result + usb3Amount;
        result = 31 * result + (networkInterface != null ? networkInterface.hashCode() : 0);
        result = 31 * result + (AudioController != null ? AudioController.hashCode() : 0);
        result = 31 * result + (formFactor != null ? formFactor.hashCode() : 0);
        result = 31 * result + (power != null ? power.hashCode() : 0);
        return result;
    }

    public String getRamType() {
        return ramType;
    }



    public void setRamType(String ramType) {
        this.ramType = ramType;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public void setChipset(String chipset) {
        this.chipset = chipset;
    }

    public void setDoubleVideoCard(String doubleVideoCard) {
        this.doubleVideoCard = doubleVideoCard;
    }

    public void setDdr3Amount(int ddr3Amount) {
        this.ddr3Amount = ddr3Amount;
    }

    public void setDdr4Amount(int ddr4Amount) {
        this.ddr4Amount = ddr4Amount;
    }

    public void setPciAmount(int pciAmount) {
        this.pciAmount = pciAmount;
    }

    public void setPciEX1Amount(int pciEX1Amount) {
        this.pciEX1Amount = pciEX1Amount;
    }

    public void setPciE20x16Amount(int pciE20x16Amount) {
        this.pciE20x16Amount = pciE20x16Amount;
    }

    public void setPciE30x16Amount(int pciE30x16Amount) {
        this.pciE30x16Amount = pciE30x16Amount;
    }

    public void setSata2Amount(int sata2Amount) {
        this.sata2Amount = sata2Amount;
    }

    public void setSata3Amount(int sata3Amount) {
        this.sata3Amount = sata3Amount;
    }

    public void setPs2Amount(int ps2Amount) {
        this.ps2Amount = ps2Amount;
    }

    public void setUsb2Amount(int usb2Amount) {
        this.usb2Amount = usb2Amount;
    }

    public void setUsb3Amount(int usb3Amount) {
        this.usb3Amount = usb3Amount;
    }

    public void setNetworkInterface(String networkInterface) {
        this.networkInterface = networkInterface;
    }

    public void setAudioController(String audioController) {
        AudioController = audioController;
    }

    public void setFormFactor(String formFactor) {
        this.formFactor = formFactor;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getSocket() {
        return socket;
    }

    public String getChipset() {
        return chipset;
    }

    public String getDoubleVideoCard() {
        return doubleVideoCard;
    }

    public int getDdr3Amount() {
        return ddr3Amount;
    }

    public int getDdr4Amount() {
        return ddr4Amount;
    }

    public int getPciAmount() {
        return pciAmount;
    }

    public int getPciEX1Amount() {
        return pciEX1Amount;
    }

    public int getPciE20x16Amount() {
        return pciE20x16Amount;
    }

    public int getPciE30x16Amount() {
        return pciE30x16Amount;
    }

    public int getSata2Amount() {
        return sata2Amount;
    }

    public int getSata3Amount() {
        return sata3Amount;
    }

    public int getPs2Amount() {
        return ps2Amount;
    }

    public int getUsb2Amount() {
        return usb2Amount;
    }

    public int getUsb3Amount() {
        return usb3Amount;
    }

    public String getNetworkInterface() {
        return networkInterface;
    }

    public String getAudioController() {
        return AudioController;
    }

    public String getFormFactor() {
        return formFactor;
    }

    public String getPower() {
        return power;
    }
}
