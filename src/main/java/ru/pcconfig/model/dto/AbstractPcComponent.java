package ru.pcconfig.model.dto;

/**
 * Created by Ruslan on 11.03.2017.
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import ru.pcconfig.model.priceParse.Price;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@MappedSuperclass
public abstract class AbstractPcComponent implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    protected int id;
    @Column
    protected String producer;
    @Column
    protected String name;
    @Transient
    protected Price price;
    @Transient
    protected Map parametersForParsing;
    @Transient
    protected Map parametersForFullDescription;
    @Transient
    protected Map parametersForShortDescription;

    public Map getParametersForParsing() {
        return parametersForParsing;
    }

    public void setAllParams(AbstractPcComponent pcComponent){
        List<Field> fields = new ArrayList<>();
        //Field[] fields = pcComponent.getClass().getDeclaredFields();
        fields.addAll(Arrays.asList(pcComponent.getClass().getDeclaredFields()));
        fields.addAll(Arrays.asList(pcComponent.getClass().getSuperclass().getDeclaredFields()));
        for (int i = 0; i < fields.size(); i++) {
            fields.get(i).setAccessible(true);
            if(fields.get(i).getAnnotationsByType(Column.class).length>0){
                try {
                    fields.get(i).set(this, fields.get(i).get(pcComponent));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        this.price = pcComponent.price;
    }

    public void setParametersForParsing(Map parametersForParsing) {
        this.parametersForParsing = parametersForParsing;
    }

    public Map getParametersForFullDescription() {
        return parametersForFullDescription;
    }

    public void setParametersForFullDescription(Map parametersForFullDescription) {
        this.parametersForFullDescription = parametersForFullDescription;
    }

    public Map getParametersForShortDescription() {
        return parametersForShortDescription;
    }

    public void setParametersForShortDescription(Map parametersForShortDescription) {
        this.parametersForShortDescription = parametersForShortDescription;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducer() {
        return producer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
}
