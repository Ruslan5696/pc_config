package ru.pcconfig.model.dto;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ruslan on 12.03.2017.
 */
@Component
@Entity
@Table(name = "hdd")
public class Hdd extends AbstractPcComponent{
    @Column
    private String formFactor;
    @Column
    private int memorySize; //Гб
    @Column
    private String interfaceConnection;
    @Column
    private int bufferSize; //Мб
    @Column
    private int rotationSpeed;



    @Override
    public String toString() {
        return name;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hdd hdd = (Hdd) o;

        if (memorySize != hdd.memorySize) return false;
        if (bufferSize != hdd.bufferSize) return false;
        if (rotationSpeed != hdd.rotationSpeed) return false;
        if (formFactor != null ? !formFactor.equals(hdd.formFactor) : hdd.formFactor != null) return false;
        return interfaceConnection != null ? interfaceConnection.equals(hdd.interfaceConnection) : hdd.interfaceConnection == null;
    }

    @Override
    public int hashCode() {
        int result = formFactor != null ? formFactor.hashCode() : 0;
        result = 31 * result + memorySize;
        result = 31 * result + (interfaceConnection != null ? interfaceConnection.hashCode() : 0);
        result = 31 * result + bufferSize;
        result = 31 * result + rotationSpeed;
        return result;
    }

    public String getFormFactor() {
        return formFactor;
    }

    public void setFormFactor(String formFactor) {
        this.formFactor = formFactor;
    }

    public int getMemorySize() {
        return memorySize;
    }

    public void setMemorySize(int memorySize) {
        this.memorySize = memorySize;
    }

    public String getInterfaceConnection() {
        return interfaceConnection;
    }

    public void setInterfaceConnection(String interfaceConnection) {
        this.interfaceConnection = interfaceConnection;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public int getRotationSpeed() {
        return rotationSpeed;
    }

    public void setRotationSpeed(int rotationSpeed) {
        this.rotationSpeed = rotationSpeed;
    }
}
