package ru.pcconfig.model.dto;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by Ruslan on 12.03.2017.
 */
@Component
@Entity
@Table(name = "video_card")
public class VideoCard extends AbstractPcComponent {

    @Column
    private String interfaceConnection;
    @Column
    private String videoChipset;
    @Column
    private int techProcess; //нм
    @Column
    private int frequency; //Мгц
    @Column
    private int frequencyBoost;
    @Column
    private int memorySize; //Мб
    @Column
    private String power;
    @Column
    private String memoryType;
    @Column
    private int busDepth;
    @Column
    private String doubleVideoCard;
    @Column
    private int recommendedPower; //Вт


    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VideoCard videoCard = (VideoCard) o;

        if (techProcess != videoCard.techProcess) return false;
        if (frequency != videoCard.frequency) return false;
        if (frequencyBoost != videoCard.frequencyBoost) return false;
        if (memorySize != videoCard.memorySize) return false;
        if (busDepth != videoCard.busDepth) return false;
        if (recommendedPower != videoCard.recommendedPower) return false;
        if (interfaceConnection != null ? !interfaceConnection.equals(videoCard.interfaceConnection) : videoCard.interfaceConnection != null)
            return false;
        if (videoChipset != null ? !videoChipset.equals(videoCard.videoChipset) : videoCard.videoChipset != null)
            return false;
        if (power != null ? !power.equals(videoCard.power) : videoCard.power != null) return false;
        if (memoryType != null ? !memoryType.equals(videoCard.memoryType) : videoCard.memoryType != null) return false;
        return doubleVideoCard != null ? doubleVideoCard.equals(videoCard.doubleVideoCard) : videoCard.doubleVideoCard == null;
    }

    @Override
    public int hashCode() {
        int result = interfaceConnection != null ? interfaceConnection.hashCode() : 0;
        result = 31 * result + (videoChipset != null ? videoChipset.hashCode() : 0);
        result = 31 * result + techProcess;
        result = 31 * result + frequency;
        result = 31 * result + frequencyBoost;
        result = 31 * result + memorySize;
        result = 31 * result + (power != null ? power.hashCode() : 0);
        result = 31 * result + (memoryType != null ? memoryType.hashCode() : 0);
        result = 31 * result + busDepth;
        result = 31 * result + (doubleVideoCard != null ? doubleVideoCard.hashCode() : 0);
        result = 31 * result + recommendedPower;
        return result;
    }

    public void setInterfaceConnection(String interfaceConnection) {
        this.interfaceConnection = interfaceConnection;
    }

    public void setVideoChipset(String videoChipset) {
        this.videoChipset = videoChipset;
    }

    public void setTechProcess(int techProcess) {
        this.techProcess = techProcess;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void setFrequencyBoost(int frequencyBoost) {
        this.frequencyBoost = frequencyBoost;
    }

    public void setMemorySize(int memorySize) {
        this.memorySize = memorySize;
    }

    public void setMemoryType(String memoryType) {
        this.memoryType = memoryType;
    }

    public void setBusDepth(int busDepth) {
        this.busDepth = busDepth;
    }

    public void setDoubleVideoCard(String doubleVideoCard) {
        this.doubleVideoCard = doubleVideoCard;
    }

    public void setRecommendedPower(int recommendedPower) {
        this.recommendedPower = recommendedPower;
    }

    public String getInterfaceConnection() {
        return interfaceConnection;
    }

    public String getVideoChipset() {
        return videoChipset;
    }

    public int getTechProcess() {
        return techProcess;
    }

    public int getFrequency() {
        return frequency;
    }

    public int getFrequencyBoost() {
        return frequencyBoost;
    }

    public int getMemorySize() {
        return memorySize;
    }

    public String getMemoryType() {
        return memoryType;
    }

    public int getBusDepth() {
        return busDepth;
    }

    public String getDoubleVideoCard() {
        return doubleVideoCard;
    }

    public int getRecommendedPower() {
        return recommendedPower;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }
}
